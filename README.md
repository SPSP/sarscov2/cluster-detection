# SNP Cluster detection tool

This tool computes sequence clusters based on the SNP differences between the input sequences following a 3 step process: 
    
1. Find groups of sequences that are at a 0-SNP distance between each other.
2. Augment the 0-SNP groups by adding sequences with 1-SNP difference. This will result in non exclusive clusters of sequences.

![1SNP clusters](https://gitlab.sib.swiss/SPSP/sarscov2/cluster-detection/-/blob/master/pics/1snp_clusters.png)

3. Merge the 1-SNP clusters that have overlapping sequences using hierarchical clustering.

![Hierarchical Clustering](https://gitlab.sib.swiss/SPSP/sarscov2/cluster-detection/-/blob/master/pics//hierarchical_clustering.png)

An additional functionality that this tool offers is the possibility to compare a set of query sequences with the base ones. This means that the output will only show the clusters which contain query sequences.

This tool has been developped in python3 and can be used from a python script or terminal. Have a look at the following section to know how.

# Table of contents
1. [Quickstart](#quickstart)
    
    1.1 [Installation](#install)
    
    1.2 [How to use it](#use)
        
    1.2.1 [From a python file/notebook](#fromfile)

    1.2.1 [From terminal](#fromterm)

2. [Inputs](#inputs)
    
3. [Outputs](#outputs)

4. [Troubleshooting](#troubleshoot)

5. [License](#license)





## 1. Quickstart <a name="quickstart"></a>

### 1.1 Installation <a name="install"></a>

1.  First of all, make sure that you have anaconda installed in your compter. Otherwise, install it following the instructions here: https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html

2. Clone the GitLab repository by copying the following command in your terminal:

    `git clone https://gitlab.sib.swiss/SPSP/sarscov2/cluster-detection.git`

3.  Go to the cloned directory /cluster-detection:

    `cd cluster-detection/pangolin`

4. Create a new python environment using the following command, the new environment will be called gisaid_clusters. This step may take a couple of minutes:

    `conda env create -f environment.yml`

5. Activate the newly created environment (snp_clusters) pasting this command on your terminal:

    `conda activate snp_clusters`

6. Install the dependencies using:

    `pip install .`

7. Go to cluster_detection_tool to install the tool
    `cd ..`
    `cd cluster_detection_tool`
    `pip install .`

***Note: once the package is published in pip and anaconda these steps will not be needed, but for now we need to install the packages locally.*** 

Congratulations! You are ready to start finding new sequence clusters :)

### 1.2 How to use it <a name="use"></a>
In this section we will explain how to use the cluster tool from a python script

#### 1.2.1 From a python file/notebook <a name="fromfile"></a>
Once all the dependencies have been successfully installed:

1. Activate conda environment created previously (snp_clusters) if not active yet.

2. Import get_snp_clusters module:

    ```
    from get_snp_clusters.get_clusters import get_snp_clusters
    ```

3. Define the inputs

In this example we are not going to query any samples. To know the options for each parameter and what do they mean go to the section __Inputs__.

    
    inputs = {"LOCATION": None, 
          "CLUSTER_SIZE_LIMIT": [3,1000000000000000000], 
          "DAYS_SPAN_LIMIT": [0, 20], 
          "DATE_SPAN_LIMIT": ["1900-01-01", "2200-12-31"],
          "OUTPUT_DIRECTORY":  "/Users/Blanca/Documents/SIB/SPSP/cluster-detection",
          "ANALYSIS_NAME": "analysis_name",
          "PANGOLIN_FILE": "path_to_file_with_pangolin_inforamtion",
          "NEXTCLADE_FILE": "path_to_file_with_nextclade_inforamtion",
          "SAMPLES_METADATA_FILE": "path_to_file_with_metadata_inforamtion",
          "SEQUENCES_TO_QUERY": None,
          "METADATA_TO_QUERY": None,
          "FASTA_PATTERN": "*.fasta*",
          "MERGE_CLUSTERS": True,
          "LOG_SCALE": False,
          "SAMPLES": None}
    
4. Run the tool.
    ```
    get_snp_clusters(inputs)
    ```

#### 1.2.2 From terminal <a name="fromterm"></a>

Once all the dependencies have been successfully installed:

1. Activate conda environment if not active yet:

    `conda activate snp_clusters`

2. Fill in the inputs.yaml according to your needs. For questions related to what each input means take a look at the readme section __Inputs__.



3. On your terminal, go to the directory /cluster-detection-master if downloaded from a zip file, or /cluster-detection if cloned from git (the main project directory):

    `cd cluster-detection-master`

    or 

    `cd cluster-detection`


4. Type this command in your terminal

    `python get_snp_clusters/get_clusters.py`


---

## 2. Inputs <a name="inputs"></a>
The inputs of the script are taken from the _inputs.yaml_ file located in the _cluster_detection_ directory, this means that this file is the only one that you need to modify to perform your analysis. Inside this file you will find the following variables:

- __LOCATION:__ This variable is used to filter the sequences by canton. Enter the Swiss canton code (CHE/Code) to retrieve its sequences, eg: "CHE/BS" for Basel. To not filter by location set it to null.

- __CLUSTER_SIZE_LIMIT:__ This variable is used to filter the output clusters depending on the number of sequences they contain. If [5, 10] are the inputs, then the script will return only the clusters containing a minimum of 5 and maximum of 10 sequences, limits included. To not filter by cluster size set it to [0, 100000000000].

- __DAYS_SPAN_LIMIT:__ This variable is used to filter the clusters according to the days between the first and the latest sequence isolation dates. If we set it to [2, 15], the script will return the clusters which sequences have been isolated within a time difference of minimum 2 days and maximum 15 between the first and the last sequences. To not filter by days set it to [0, 100000000000].

- __DATE_SPAN_LIMIT:__ This variable is used to filter the sequences by date. This means that only de sequences isolated in the period between the stated dates will be used for the analysis. Example: If we set this variable to ["2020-11-15", "2021-08-30"], only the sequences isolated on/after the 15th of November 2020 and before/on the 20th of August 2021 will be used. To not filter by date set this variable to "1900-01-01" and "2200-12-31".

- __OUTPUT_DIRECTORY:__ Path to the directory where the output folder is or will be located. If set to null, the script will take the current directory as output directory.

- __ANALYSIS_NAME:__ Directory name within OUTPUT_DIRECTORY where the results of the current analysis will be stored. If set to null a directory named with the date and time (yyyymmdd-hhmmss) will be created.

- __SAMPLES_METADATA_FILE:__ Directory where the samples metadata file (.tsv or .csv)containing the information of the sequences to process is located.

- __DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:__ Variable used to state the directory where the sequences to query are to be located. The script accepts two types of inpututs:

    - Path to a directory that contains individual .fasta files: The script will look for all the .fasta files in the stated directory, create a multifasta.fasta file and run nextclade and pangolin on them. Example: "path/to/directory/"

    - Path to a .fasta file containing all the sequences to query. The script will run nextclade and pangolin on them. Example: "path/to/multifasta.fasta"

    - None if no sequences need to be queried.

- __LOG_SCALE:__ Use a log scale for the x-axis of the lineage scatter plot. Example: True or False.

- __MERGE_CLUSTERS__: Merge the 1-SNP clusters. Set to true or false according to your needs. Example: True or False.


The metadata files need to have the following columns:
    
- sample.strain_name
- sample.isolation_date
- sample_origin_location.country_division
- assembly.annotation_pangolin.lineage
- assembly.annotation_pangolin.scorpio_call
- assembly.annotation_next_clade.clade
- assembly.annotation_next_clade.substitutions



---


## 3. Outputs <a name="outputs"></a>
Once the tool has finished executing, you will find the results in the directory stated in the inputs: [OUTPUT_DIRECTORY]/[ANALYSIS_NAME]

- __potential_outbreaks_summary_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x.tsv:__ Table containing the summary information per cluster. The columns containing a (0) contain the information of the 0-SNP clusters, the columns containing a (1) contain the information of the 1-SNP clusters. The query column tells us whether the cluster contains sequences belonging to the query sequences [1] or its sequences are from the base sequences [0]. There are some columns withot information (start_date, end_date and cantons) as we don't have this information for the query sequences.
The name indicates: NC as cluster size, D as date, TW as time window, LOC as location.

- __potential_outbreaks_summary_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x_all_sequences_snp.tsv:__ Table containing the analysis information for all the individual sequences that were not filtered and, if there were query sequences, they belong to a 0-SNP cluster with a query sequence. The query column of the table tells us whether the sequence is a query sequence [1] or if it is a sequence from the SPSP contextual sequences [0]. There may be some columns withot information (start_date, end_date and cantons) if the metadata for the query sequences was not set. 
The name indicates: NC as cluster size, D as date, TW as time window, LOC as location.

- __potential_outbreaks_summary_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x_all_sequences_merged.tsv:__ This table will only appear if the MERGE_CLUSTERS input is set to true. Table containing the analysis information for all the individual sequences that were not filtered and, if there were query sequences, they belong to a merged cluster with a query sequence. The query column of the table tells us whether the sequence is a query sequence [1] or if it is a sequence from the SPSP contextual sequences [0]. There may be some columns withot information (start_date, end_date and cantons) if the metadata for the query sequences was not set. 
The name indicates: NC as cluster size, D as date, TW as time window, LOC as location.

- __clusters_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x:__ Directory where the tables containing the information per cluster is stored. Each table in this directory contains the information of the sequences that belong to the cluster stated in the table name under *_CL_x.tsv*. In these tables the column __SNP_distance__ indicates if the sequences are at 0 or 1 SNP distance from each other.

- __cluster_lineage_scatter_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x.png:__ Scatter plot providing a visual interpretation of the number of clusters and the average number of sequences per cluster, per lineage. In the plot each point corresponds to a lineage. The x axis indicates the number of clusters belonging to that lineage, the y axis indicates the average number of sequences assigned to each 0-SNP cluster. A black line going upwards from a lineage point indicates the new average number of sequences belonging to the 1-SNP clusters. A random jitter of 0,+0.5 has been added to both x and y coordinates to allow a better visualization and avoid overlapped lineages. Set the input __LOG_SCALE__ to *True* to change the x axis to logarithmic scale.

- __cluster_lineage_barplots_NC_x-x_D_yyyy-mm-dd-yyyy-mm-dd_TW_x-x_LOC_x:__ Summary of the sequence-cluster-lineage information. The upper plot represents the distribution of the sequences per cluster in each lineage. The lower barplot represents the number of clusters corresponding to each lineage. This plot will show the 6 lineages with the maximum number of clusters.

- __lineage_report.csv:__ Pangolin output

- __output_nextclade:__ Output from nextclade


## 4. Troubleshooting <a name="troubleshoot"></a>

For any bug, issue or problem that you may encounter at any stage of using this script, please send us an email at spsp-support@sib.swiss.

## 5. License <a name="license"></a>

The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.
