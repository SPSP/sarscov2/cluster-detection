import os
import itertools
import matplotlib
import numpy as np
import pandas as pd
from tqdm import tqdm

from dateutil.relativedelta import relativedelta
from datetime import date, timedelta

from scipy.stats import iqr

from get_snp_clusters.utils import compute_snp_differences

from geopy.distance import distance

# Methods used in the notebook SPSP-paper-plots.ipynb


## Create axis methods
# Week order: Create a list of year/weeks for a time period to be the ticks of the plot
def create_axis(start, end, max_value=53, min_value=0):

    start_year, start_month = start.split("-")
    end_year, end_month = end.split("-")

    start_year_int = int(start_year)
    end_year_int = int(end_year)
    start_month_int = int(start_month)
    end_month_int = int(end_month)

    ordered_weeks = []

    for y in range((end_year_int + 1) - start_year_int):
        year = start_year_int + y

        if year == start_year_int:
            temp_dates = [
                "{}-{}".format(year, d) for d in np.arange(start_month_int, max_value)
            ]
            ordered_weeks += temp_dates

        elif year == end_year_int:
            temp_dates = [
                "{}-{}".format(year, d) for d in np.arange(min_value, end_month_int)
            ]
            ordered_weeks += temp_dates

        else:
            temp_dates = [
                "{}-{}".format(year, d) for d in np.arange(min_value, max_value)
            ]
            ordered_weeks += temp_dates

    return ordered_weeks


# Week order: Create a list of year/weeks for a time period to be the ticks of the plot
def create_axis_week(start, end):

    start_year = int(start.split("-")[0])
    start_week = int(start.split("-")[1])
    end_year = int(end.split("-")[0])
    end_week = int(end.split("-")[1])

    ordered_weeks = []

    start_date = date(start_year, 1, 1) + relativedelta(weeks=+start_week)
    end_date = date(end_year, 1, 1) + relativedelta(
        weeks=+end_week
    )  # perhaps date.now()

    delta = end_date - start_date  # returns timedelta
    
    previous_week = None
    for i in range(delta.days + 1):
        day = start_date + timedelta(days=i)
        week = day.strftime("%y-%W")
        if week == previous_week:
            continue
        ordered_weeks.append(week)
        previous_week = week

    return ordered_weeks


## Find neighbour sequences
def find_neighbours(row, df, colname, clusters_col="merged_clusters"):
    value = row.median_isolation_date
    cluster = row.merged_clusters
    exactmatch = df[(df[colname] == value) & (df[clusters_col] == cluster)]

    if not exactmatch.empty:
        return exactmatch[colname].values[0]
    else:
        lowerneighbour_ind = df[(df[colname] < value) & (df[clusters_col] == cluster)][
            colname
        ].idxmax()
        if isinstance(lowerneighbour_ind, np.int64):
            return df.iloc[lowerneighbour_ind][colname]

        upperneighbour_ind = df[(df[colname] > value) & (df[clusters_col] == cluster)][
            colname
        ].idxmin()
        if isinstance(upperneighbour_ind, np.int64):
            return df.iloc[upperneighbour_ind][colname]


## Get cluster time-span
def get_time_span(
    sequences,
    date_col="sample.isolation_date",
    clusters_col="merged_clusters",
    seq_id_col="sample.strain_name",
):

    seq_df = sequences.copy()

    # Convert dates to datetime
    seq_df[date_col] = pd.to_datetime(seq_df[date_col], format="%Y-%m-%d")
    seq_df[date_col] = seq_df.apply(lambda row: row[date_col].normalize(), axis=1)

    # print(seq_df.head())

    # Find min, max, and median isolaton dates for each cluster
    median_date = (
        seq_df[[clusters_col, date_col, seq_id_col]]
        .groupby(by=clusters_col)[date_col]
        .agg(lambda x: x.median())
        .reset_index()
        .rename(columns={date_col: "median_isolation_date"})
    )
    max_date = (
        seq_df[[clusters_col, date_col]]
        .groupby(by=clusters_col)
        .max()
        .reset_index()
        .rename(columns={date_col: "last_isolation_date"})
    )
    min_date = (
        seq_df[[clusters_col, date_col]]
        .groupby(by=clusters_col)
        .min()
        .reset_index()
        .rename(columns={date_col: "first_isolation_date"})
    )
    median_date["median_isolation_date"] = median_date.apply(
        lambda row: row["median_isolation_date"].normalize(), axis=1
    )

    # Find median sequence
    median_date["sample_median_isolation_date"] = median_date.apply(
        find_neighbours, df=seq_df, colname=date_col, axis=1
    )

    # Add median cluster strain name
    cluster_seg_to_merge = seq_df[[seq_id_col, clusters_col, date_col]].rename(
        columns={"clusters": clusters_col, date_col: "sample_median_isolation_date"}
    )
    median_date_seq = median_date.merge(
        cluster_seg_to_merge,
        on=[clusters_col, "sample_median_isolation_date"],
        how="left",
    )
    median_date_seq = median_date_seq.drop_duplicates(
        subset=[clusters_col, "sample_median_isolation_date"]
    )

    # Add first isolation date to dataframe
    cluster_seq_dates = seq_df.merge(
        median_date_seq, on=clusters_col, how="outer"
    ).rename(
        columns={seq_id_col + "_y": "median_sample", seq_id_col + "_x": seq_id_col}
    )
    cluster_seq_dates = cluster_seq_dates.merge(
        max_date[[clusters_col, "last_isolation_date"]], on=clusters_col, how="outer"
    )
    cluster_seq_dates = cluster_seq_dates.merge(min_date, on=clusters_col, how="outer")

    # Count the number of days since the first isolation days
    cluster_seq_dates["days_span"] = (
        cluster_seq_dates[date_col] - cluster_seq_dates["median_isolation_date"]
    )
    cluster_seq_dates["days"] = cluster_seq_dates.apply(
        lambda row: row["days_span"].days, axis=1
    )

    # Compute iqr50
    iqr_df = pd.DataFrame(columns=[clusters_col, "iqr"])

    for cluster in cluster_seq_dates[clusters_col].unique():
        cluster_seq = cluster_seq_dates.loc[cluster_seq_dates[clusters_col] == cluster]
        days_distribution = cluster_seq.days.values
        iqr_value = iqr(days_distribution)
        iqr_df = pd.concat(
            [iqr_df, pd.DataFrame(data={clusters_col: [cluster], "iqr": [iqr_value]})],
            ignore_index=True,
        )

    cluster_seq_dates = cluster_seq_dates.merge(iqr_df, on=clusters_col, how="left")

    return cluster_seq_dates


## Initial sequence data processing
def process_seq_data(df, spsp_df, max_date=15, compute_snp_difference = True):

    # Get time span
    df_time = get_time_span(df)

    # Add VoC info
    df_time_snp = df_time.merge(
        spsp_df[
            [
                "sample.strain_name",
                "assembly.annotation_next_clade.substitutions",
                "assembly.annotation_pangolin.scorpio_call",
            ]
        ],
        on="sample.strain_name",
        how="left",
    )
    
    df_time_snp = df_time_snp.rename(
        columns={
            "assembly.annotation_next_clade.substitutions": "sequence_substitutions"
        }
    )

    df_time_snp["week"] = df_time_snp.apply(
        lambda row: row["sample.isolation_date"].strftime("%y-%W"), axis=1
    )

    # Add median sequence substitutions
    df_time_snp_median = df_time_snp.merge(
        spsp_df[
            ["sample.strain_name", "assembly.annotation_next_clade.substitutions"]
        ].rename(
            columns={
                "sample.strain_name": "median_sample",
                "assembly.annotation_next_clade.substitutions": "median_sequence_substitutions",
            }
        ),
        on="median_sample",
        how="left",
    )
    
    df_time_snp_median["sequence_substitutions"] = df_time_snp_median["sequence_substitutions"].fillna("")
    df_time_snp_median["median_sequence_substitutions"] = df_time_snp_median["median_sequence_substitutions"].fillna("")

    if compute_snp_difference:
        # Compute snp differences
        df_time_snp_median["snp_differences"] = df_time_snp_median.apply(
            lambda row: compute_snp_differences(
                row.sequence_substitutions.split(","),
                row.median_sequence_substitutions.split(","),
            ),
            axis=1,
        )

    # Add VoC to sequences
    df_time_snp_median = add_voc_from_scorpio(df_time_snp_median)
    

    # Get good clusters
    df_time_snp_bad_cl = df_time_snp_median.loc[
        (df_time_snp_median.days > max_date) | (df_time_snp_median.days < -max_date)
    ].merged_clusters.unique()

    # Get sequences belonging to the <15 days clusters
    df_time_snp_good = df_time_snp_median.loc[
        ~df_time_snp_median.merged_clusters.isin(df_time_snp_bad_cl)
    ]

    # Get sequences belonging to the >15 days clusters
    df_time_snp_bad = df_time_snp_median.loc[
        df_time_snp_median.merged_clusters.isin(df_time_snp_bad_cl)
    ]

    return df_time_snp_good, df_time_snp_bad

## Add voc to df

def add_voc_from_scorpio(df, column="assembly.annotation_pangolin.scorpio_call", new_column_name="VoC"):
    # Add VoC to sequences
    df_temp = df.copy()
    df_temp[new_column_name] = "unassigned"
    df_temp[new_column_name] = df_temp.apply(
        lambda row: row[column].split()[0]
        if isinstance(row[column], str)
        else "unassigned",
        axis=1,
    )
    df_temp[new_column_name] = df_temp.apply(
        lambda row: row[column].split()[1]
        if row[new_column_name] == "Probable"
        else row[new_column_name],
        axis=1,
    )
    df_temp[new_column_name] = df_temp.apply(
        lambda row: "other" if "like" in row[new_column_name] else row[new_column_name], axis=1
    )
    return df_temp



## Get maximum distance between cantons
def get_max_distance_cantons(
    df,
    canton_coordinate_dict,
    column_clusters="merged_clusters",
    column_cantons="sample_origin_location.country_division",
):

    # Get maximum distance between cantons
    canton_distance_cluster = {}
    for cluster in df[column_clusters].unique():
        sample = df.loc[df[column_clusters] == cluster]
        unique_cantons = sample[column_cantons].unique().tolist()
        if "-" in unique_cantons:
            unique_cantons.remove("-")

        if len(unique_cantons) == 1:
            canton_distance_cluster[cluster] = 0
            continue

        # create all possible combinations between clusters
        combinations = itertools.combinations(unique_cantons, 2)

        max_distance = 0
        for combi in combinations:
            if combi[0] not in list(canton_coordinate_dict.keys()):
                print("%s skipped \n" %combi[0])
                continue
            elif combi[1] not in list(canton_coordinate_dict.keys()):
                print("%s skipped \n" %combi[1])
                continue
            dist = distance(
                canton_coordinate_dict[combi[0]], canton_coordinate_dict[combi[1]]
            ).km

            if dist > max_distance:
                max_distance = dist
        canton_distance_cluster[cluster] = max_distance

    return canton_distance_cluster


## Get first detection dates
def get_first_detection_voc(
    samples,
    voc,
    column_voc="assembly.annotation_pangolin.scorpio_call",
    date_column="sample.isolation_date",
):
    samples_voc = samples.loc[samples[column_voc].str.contains(voc)]
    min_date = samples_voc[date_column].min()

    return min_date


def get_first_detection_all_vocs(samples, voc_list, **kwargs):
    voc_first_dict = {}
    for voc in voc_list:
        voc_first_dict[voc] = get_first_detection_voc(samples, voc, **kwargs)

    return voc_first_dict


def get_voc_first_date(
    subsamples_dir, sub, boot, downsample_size, vocs, j, file_to_load, dump
):
    try:

        file = os.path.join(subsamples_dir, sub, boot, file_to_load)

        cluster_results = pd.read_csv(file, sep="\t")
    except:
        print("For boot {} there where no sequence left for clustering".format(boot))
        return "break"
    
    # Filter bad clusters
    # Get sequence data information
    data_info, data_info_over_max_date = process_seq_data(cluster_results, dump, max_date=15, compute_snp_difference=False)
    
    data_info[
        "assembly.annotation_pangolin.scorpio_call"
    ] = data_info["assembly.annotation_pangolin.scorpio_call"].fillna(" ")
    data_info[
        "sequence_substitutions"
    ] = data_info["sequence_substitutions"].fillna(" ")
    

    # Get dataset for
    voc_first_dates = get_first_detection_all_vocs(data_info, vocs)
    

    return {
        "subsampling": [int(downsample_size)] * len(vocs),
        "id": [j] * len(vocs),
        "date": list(voc_first_dates.values()),
        "VoC": list(voc_first_dates.keys()),
    }


## Compute delay in VoC detection
def compute_delay(row, dates_df, column_dates="sequence_dates"):
    voc = row.VoC
    delay = row.date - dates_df.loc[dates_df.VoC == voc][column_dates].values[0]
    return delay.days


## Compute delay in cluster detection
def get_cluster_delay(
    subsamples_dir, sub, boot, downsample_size, j, file_to_load, dump
):
    try:

        file = os.path.join(subsamples_dir, sub, boot, file_to_load)

        cluster_results = pd.read_csv(file, sep="\t")
    except:
        print("For boot {} there where no sequence left for clustering".format(boot))
        return "break"

    cluster_results["sample.isolation_date"] = pd.to_datetime(
        cluster_results["sample.isolation_date"], format="%Y-%m-%d"
    )
    cluster_results["sample.isolation_date"] = cluster_results.apply(
        lambda row: row["sample.isolation_date"].normalize(), axis=1
    )

    max_date = (
        cluster_results[["merged_clusters", "sample.isolation_date"]]
        .groupby(by="merged_clusters")
        .max()
        .reset_index()
        .rename(columns={"sample.isolation_date": "last_isolation_date"})
    )
    min_date = (
        cluster_results[["merged_clusters", "sample.isolation_date"]]
        .groupby(by="merged_clusters")
        .min()
        .reset_index()
        .rename(columns={"sample.isolation_date": "first_isolation_date"})
    )
    cluster_results = cluster_results.merge(
        max_date[["merged_clusters", "last_isolation_date"]],
        on="merged_clusters",
        how="outer",
    )
    cluster_results = cluster_results.merge(min_date, on="merged_clusters", how="outer")

    # Count the number of days since the first isolation days
    # cluster_results["days_span"] = cluster_results["sample.isolation_date"] - cluster_seq_dates["median_isolation_date"]
    # cluster_results["days"] = cluster_results.apply(lambda row: row["days_span"].days, axis=1)

    # cluster_results_days = get_time_span(cluster_results)
    cluster_results = cluster_results.merge(
        dump[["sample.strain_name", "assembly.annotation_pangolin.scorpio_call"]],
        on="sample.strain_name",
        how="left",
    )
    cluster_results["assembly.annotation_pangolin.scorpio_call"] = cluster_results[
        "assembly.annotation_pangolin.scorpio_call"
    ].fillna(" ")

    grouped_clusters = cluster_results.drop_duplicates(subset="merged_clusters")
    grouped_clusters["days_span"] = grouped_clusters.apply(
        lambda row: row.last_isolation_date - row.first_isolation_date, axis=1
    )
    grouped_clusters["days"] = grouped_clusters.apply(
        lambda row: row["days_span"].days, axis=1
    )
    len_clusters = len(grouped_clusters)

    return {
        "subsampling": [int(downsample_size)] * len_clusters,
        "id": [j] * len_clusters,
        "date": list(grouped_clusters.days.values),
        "VoC": list(
            grouped_clusters["assembly.annotation_pangolin.scorpio_call"].values
        ),
    }


## Normalize cluster counts by a moving time window
def cluster_time_window_norm(row, tw, df, column_name_df = "first_week_day", column_name_row = "first_week_day", column_counts = "cluster_counts"):
  
    sec_per_day = (
        df.loc[
            (df[column_name_df] > (row[column_name_row] - np.timedelta64(tw, "D")))
            & (df[column_name_df] < (row[column_name_row] + np.timedelta64(tw, "D")))
        ]
        #.drop_duplicates(subset=column_name)
        .sequences.sum()
    )
   
    return row[column_counts] / sec_per_day


## Add random gitter
def rand_jitter(arr):
    stdev = 0.01 * (max(arr) - min(arr))
    return arr + np.random.randn(len(arr)) * stdev


## Sort a specific dataframe column
def sorter(column):
    """Sort function"""
    ordered_weeks = create_axis_week(start="2020-07", end="2022-15")
    correspondence = {date: order for order, date in enumerate(ordered_weeks)}
    return column.map(correspondence)


## Load data from directories
def load_data_from_directories(directories_sub, vocs, main_dir, function, **kwargs):

    info_df = pd.DataFrame()

    # iterate through subsample directories
    for i, (sub) in enumerate(directories_sub):
        if sub.startswith(".") or len(sub.split(".")) > 1:
            continue

        print(sub)
        if sub.startswith("cluster"):
            downsample_size = sub.split("_")[-1]

            if downsample_size == "1e+05":
                downsample_size = 100000

            downsample_size = int(downsample_size)

            print("processing downsample " + str(downsample_size) + "\t")

            # Iterate over downsampling results
            boot_dirs = os.listdir(os.path.join(main_dir, sub))

            # iterate through bootstrap files
            for j, (boot) in enumerate(tqdm(boot_dirs)):

                if boot.startswith("."):
                    continue
                #print("boot :", boot)

                data_dict = function(
                    main_dir,
                    sub,
                    boot=boot,
                    downsample_size=downsample_size,
                    vocs=vocs,
                    j=j,
                    **kwargs
                )

                if data_dict == "break":
                    continue

                # print(voc_first_dates)
                dataframe_i = pd.DataFrame(data=data_dict)
                info_df = pd.concat([info_df, dataframe_i])

    return info_df


## Convert rgb colors to hex
def rbgtohex(rgb):
    return "#%02x%02x%02x" % rgb


## Create a degraded colormap given start and end colors
def create_colormap_colors(color_1, color_2, r=8):
    cmap_alpha = matplotlib.colors.LinearSegmentedColormap.from_list(
        "", [color_1, color_2]
    )

    color_list = []
    for i in np.arange(0, 1, 1 / r):
        color = cmap_alpha(i)
        color_int = [int(c * 255) for c in color[0:-1]]
        color_list.append(rbgtohex(tuple(color_int)))

    return color_list


## Classes

# Add an alterning white-gray grid to the plot
class GridShader:
    def __init__(self, ax, first=True, axis="x", **kwargs):
        self.spans = []
        self.sf = first
        self.ax = ax
        self.kw = kwargs
        self.ax.autoscale(False, axis=axis)
        self.axis = axis
        if self.axis == "x":
            self.cid = self.ax.callbacks.connect("xlim_changed", self.shade)
        else:
            self.cid = self.ax.callbacks.connect("ylim_changed", self.shade)
        self.shade()

    def clear(self):
        for span in self.spans:
            try:
                span.remove()
            except:
                pass

    def shade(self, evt=None):
        self.clear()
        if self.axis == "x":
            xticks = self.ax.get_xticks()
            xlim = self.ax.get_xlim()
            xticks = xticks[(xticks > xlim[0]) & (xticks < xlim[-1])]
            locs = np.concatenate(([[xlim[0]], xticks, [xlim[-1]]]))

            start = locs[1 - int(self.sf) :: 2]
            end = locs[2 - int(self.sf) :: 2]

            for s, e in zip(start, end):
                self.spans.append(self.ax.axvspan(s, e, zorder=0, **self.kw))

        elif self.axis == "y":
            yticks = self.ax.get_yticks()
            ylim = self.ax.get_ylim()
            yticks = yticks[(yticks > ylim[0]) & (yticks < ylim[-1])]
            locs = np.concatenate(([[ylim[0]], yticks, [ylim[-1]]]))

            start = locs[1 - int(self.sf) :: 2]
            end = locs[2 - int(self.sf) :: 2]

            for s, e in zip(start, end):
                self.spans.append(self.ax.axhspan(s, e, zorder=0, **self.kw))

        else:
            print("Wrong input axis. The only accepted axis are x and y.")
