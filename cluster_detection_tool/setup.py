import setuptools
setuptools.setup(name='get_snp_clusters',
version='0.1',
description='Cluster WGS sequences based on its SNP differences',
url='#',
author='Blanca Cabrera gil',
author_email='blanca.cabreragil@sib.swiss',
packages=setuptools.find_packages(),
zip_safe=False)