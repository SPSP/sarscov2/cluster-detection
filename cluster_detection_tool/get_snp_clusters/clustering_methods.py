from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
import numpy as np
from copy import deepcopy
from tqdm import tqdm
from get_snp_clusters.utils import compute_snp_differences, jaccard_index_matrix
import time
import seaborn as sns
import itertools
import scipy
import matplotlib.pyplot as plt
import pandas as pd


class Cluster:
    """
    Class for saving cluster information of >0 SNP
    """

    def __init__(self, center, nodes, index):
        self.center = center
        self.connections = nodes
        self.index = index


class MinimumSpanningTree:
    def __init__(
        self,
        df,
        column_to_compare="substitutions_list",
        column_lineage="lineage",
        mst_algorithm="kruskal",
    ):
        self.df = df
        self.adjacency_matrix = self._create_adjacency_matrix(
            self.df[column_to_compare].values
        )
        # Get lineages
        (self.lineage_values, self.lineage_categories,) = self._get_lineage_classes(
            self.df, column=column_lineage
        )
        # Create the mst
        if mst_algorithm == "kruskal":
            self.mst = self._create_mst_matrix(self.adjacency_matrix)
        elif mst_algorithm == "prims":
            self.mst = self._mst_prims(self.adjacency_matrix)
        else:
            print(
                "The selected Minimum Spanning Tree algorithm is not supported.Supported algorithms are Kruskal's (kruskal) and Prim's (prims). \n"
            )

    def _create_mst_matrix(self, adjacency_matrix):
        """
        Compute minimum spanning tree of the input adjacency matrix using Kruskal algorithm.
        :param adjacency_matrix: numpy.array containing the differences between the sequences.
        :return: numpy.array of the mst
        """
        print("Creating mst matrix. \n")
        X = csr_matrix(adjacency_matrix)
        Tcsr = minimum_spanning_tree(X)
        return Tcsr.toarray().astype(int)

    def _mst_prims(self, adjacency_matrix):
        """
        Compute minimum spanning tree of the input adjacency matrix using Prim's algorithm.
        https://www.programiz.com/dsa/kruskal-algorithm
        :param adjacency_matrix: numpy.array containing the differences between the sequences.
        :return: numpy.array of the mst
        """
        INF = 9999999
        # number of vertices in graph
        V = adjacency_matrix.shape[1]

        mst_matrix = np.zeros((adjacency_matrix.shape))
        # create for adjacency matrix to represent graph
        G = adjacency_matrix
        # create a array to track selected vertex
        selected = [0] * V
        # set number of edge to 0
        no_edge = 0
        selected[0] = True

        while no_edge < V - 1:
            # For every vertex in the set S, find the all adjacent vertices
            # , calculate the distance from the vertex selected at step 1.
            # if the vertex is already in the set S, discard it otherwise
            # choose another vertex nearest to selected vertex  at step 1.
            minimum = INF
            x = 0
            y = 0
            for i in range(V):
                if selected[i]:
                    for j in range(0 + i, V):
                        if (not selected[j]) and G[i][j]:
                            # not in selected and there is an edge
                            if minimum > G[i][j]:
                                minimum = G[i][j]
                                x = i
                                y = j
            mst_matrix[x][y] = G[x][y]
            selected[y] = True
            no_edge += 1
        return mst_matrix

    def _create_adjacency_matrix(self, column):
        """
        Given the information of a pandas.DataFrame column, compute the adjacency matrix of its rows. The adjacency
        matrix will show the differences between the different rows being no differences = 1 to ensure that all nodes
        are interconnected.
        :param column: str. pandas.DataFrame column name.
        :return: numpy.array containing the adjacency matrix of the column rows
        """
        print("Creating the adjacency matrix. \n")
        adjacency_matrix = np.zeros((len(column), len(column)))
        for i in range(len(column)):
            for j in range(0 + i, len(column)):
                subst_i = column[i]
                subst_j = column[j]
                # Compute similarities between both rows of values
                similarities = [sim for sim in subst_i if sim in subst_j]
                # Get the differences
                diff_i = len(subst_i) - len(similarities)
                diff_j = len(subst_j) - len(similarities)
                adjacency_matrix[i][j] = diff_i + diff_j + 1
        return adjacency_matrix

    def _get_lineage_classes(self, df, column="lineage"):
        """
        Get the lineage classes present in the dataset.
        :param df: pandas.DataFrame containing the information.
        :param column: str of the df column name containing the information to extract.
        :return:
            - lineage_categories.values: List with the categories for each node.
            - cat_dictionary: Dictionary mapping categorical values to integers.
        """
        print("Getting lineage classes. \n")
        lineage_categories = df[column].astype("category").cat.codes
        cat_dictionary = dict(zip(df[column], lineage_categories))
        return lineage_categories.values, cat_dictionary

    def create_adjacency_list(self, column="substitutions_list"):
        """
        Create an adjacency list based on the information of the self.df dataframe. This list represents edges as:
        (node_i, node_j, weight).
        :param column: str. self.df column name containing the information.
        :return: list of the edges.
        """
        print("Creating adjacency list. \n")
        df = self.df
        values = df[column]
        adjacency_list = []
        for i in range(len(values)):
            for j in range(0 + i, len(values)):
                subst_i = values[i]
                subst_j = values[j]
                # Compute similarities between both rows of values
                similarities = [sim for sim in subst_i if sim in subst_j]
                # Get the differences
                diff_i = len(subst_i) - len(similarities)
                diff_j = len(subst_j) - len(similarities)
                adjacency_list.append((i, j, diff_i + diff_j + 1))
        return adjacency_list

    def from_matrix_to_list(self, adjacency_matrix, limit=0):
        """
        Convert adjacency matrix to adjacency list.
        :param adjacency_matrix: np.array to convert to list.
        :param limit: int. Limit to consider that two nodes are connected.
        :return: list of the edges.
        """
        adjacency_list = []
        for i in range(adjacency_matrix.shape[0]):
            for j in range(i + 0, adjacency_matrix.shape[1]):
                if adjacency_matrix[i][j] > limit:
                    adjacency_list.append((i, j, adjacency_matrix[i][j]))
        return adjacency_list

    def _get_node_connections(self, adjacency_matrix_tree, connection, limit):
        """
        Get all the connections for the actual node
        :param adjacency_matrix_tree: numpy.array of the mst adjacency matrix
        :param connection: int. Node to analyze.
        :param limit: int. Maximum number of differences to be considered a connection.
        :return: list containing the indices of the current node's connections.
        """
        node_connections_x = list(
            np.where(
                (adjacency_matrix_tree[connection] <= limit)
                & (adjacency_matrix_tree[connection] > 0)
            )[0]
        )
        node_connections_y = list(
            np.where(
                (adjacency_matrix_tree[:, connection] <= limit)
                & (adjacency_matrix_tree[:, connection] > 0)
            )[0]
        )
        return node_connections_x + node_connections_y

    def _update_cluster_center(
        self, connections_log, clusters, cluster_count, connection
    ):
        if len(connections_log[connection]) > len(
            connections_log[clusters[cluster_count].center]
        ):
            clusters[cluster_count].center = connection
        return clusters

    def _check_connections(self, connections_info):
        """
        Check the connections of the node's connection recursively
        :param connections_info: dict containing the information of the tree's nodes
        :return: updated information of the connections
        """
        # Unpack input values
        connections_i = connections_info["node_connections"]
        connections_log = connections_info["global_connections"]
        visited = connections_info["visited"]
        adjacency_matrix = connections_info["original_graph_adjacency_matrix"]
        adjacency_matrix_tree = connections_info["mst_adjacency_matrix"]
        clusters = connections_info["clusters"]
        cluster_count = connections_info["clusters_counter"]
        clusters_assigned = connections_info["cluster_node_list"]
        limit = connections_info["distance_limit"]

        for connection in connections_i:
            # If we have already examined this connection pass
            if visited[connection]:
                continue  # we are still in the same cluster as its parent

            # If the new node is further than the limit from the cluster center then skip
            if adjacency_matrix[connection][clusters[cluster_count].center] > limit:
                continue

            clusters_assigned[connection] = cluster_count
            clusters[cluster_count].connections.append(connection)
            visited[connection] = True

            # Get new connections
            connections_log[connection] = self._get_node_connections(
                adjacency_matrix_tree, connection, limit
            )

            # Update cluster center
            clusters = self._update_cluster_center(
                connections_log, clusters, cluster_count, connection
            )

            # Prepare check_connections input
            connections_info = {
                "node_connections": connections_log[connection],
                "global_connections": connections_log,
                "visited": visited,
                "original_graph_adjacency_matrix": adjacency_matrix,
                "mst_adjacency_matrix": adjacency_matrix_tree,
                "clusters": clusters,
                "clusters_counter": cluster_count,
                "cluster_node_list": clusters_assigned,
                "distance_limit": limit,
            }
            # Do loop again for inside the connections
            (
                visited,
                clusters,
                cluster_count,
                connections_log,
                clusters_assigned,
            ) = self._check_connections(connections_info)

        return visited, clusters, cluster_count, connections_log, clusters_assigned

    def get_clusters(self, limit=1):
        """
        Get >0SNP clusters from self.adjacency_matrix.
        :param limit: int. Limit of SNP.
        :return:
            -clusters: list containing Cluster objects.
            -cluster_count: int. Count of the current cluster (for recursion).
            -clusters_assigned: dict. Keys: sequence index, Value: cluster index.
            -connections_log: dict with the connections information for each node.
        """
        print("Getting clusters. \n")
        clusters = {}
        cluster_count = 0
        clusters_assigned = {}
        connections_log = {}

        adjacency_matrix = self.adjacency_matrix
        adjacency_matrix_tree = self.mst

        visited = [False] * adjacency_matrix.shape[0]

        for i in range(adjacency_matrix.shape[0]):
            if visited[i]:
                continue
            # Get the connections for the current node
            connections_log[i] = self._get_node_connections(
                adjacency_matrix_tree, i, limit
            )

            # Update the current node status and cluster
            visited[i] = True
            clusters_assigned[i] = cluster_count
            node_connections = connections_log[i] + [i]

            # Create a new cluster
            clusters[cluster_count] = Cluster(i, node_connections, cluster_count)

            # Prepare input for check_connections()
            connections_info = {
                "node_connections": connections_log[i],
                "global_connections": connections_log,
                "visited": visited,
                "original_graph_adjacency_matrix": adjacency_matrix,
                "mst_adjacency_matrix": adjacency_matrix_tree,
                "clusters": clusters,
                "clusters_counter": cluster_count,
                "cluster_node_list": clusters_assigned,
                "distance_limit": limit,
            }

            (
                visited,
                clusters,
                cluster_count,
                connections_log,
                clusters_assigned,
            ) = self._check_connections(connections_info)
            cluster_count += 1

        return clusters, cluster_count, clusters_assigned, connections_log


def add_clusters_to_df(df, clusters_dict):
    """
    Add the found clusters to the input df.

    Args:
        df (pandas.DataFrame): DataFrame where to add the cluster values into the 'clusters' column.
        clusters (dictionary or list): Contains the cluster values. It must have the same lenght as df.

    Returns:
        pandas.DataFrame: df with the clusters column.
    """
    df_clusters = df.copy()
    assert isinstance(clusters_dict, dict), "clusters_dict must be a dictionary."

    temp_df = pd.DataFrame(
        data={
            "index": list(clusters_dict.keys()),
            "clusters": list(clusters_dict.values()),
        }
    ).set_index("index")
    df_clusters = df_clusters.merge(
        temp_df, how="left", left_index=True, right_index=True
    )

    return df_clusters


def get_0snp_clusters(df, column="substitutions_list"):
    """
    Compute 0SNP clusters from self.df
    :param column: str with the name of the values to cluster
    :return:
        - cluster_sequences: dictionary containing as keys sequences, and values the indices of the sequences having
        0 differences with it.
        - reversed_cluster_indices: dict having the equivalence between the cluster index and the OSNP sequences.
        - clusters_assigned: dict containing the sequence indices as keys and the assigned cluster index as
        values.
        - sequence_list: list containing the list representation of the cluster sequences. The position in the list (index) corresponds to its cluster value.
    """
    print("Getting 0-SNP clusters. \n")
    cluster_sequences = {}
    cluster_indices = {}
    clusters_assigned = {}
    cluster_count = 0
    sequence_list = []

    for index, row in tqdm(df.iterrows(), total=len(df)):
        seq_str = str(row[column])
        # If the sequence does exist, assign to a cluster
        if seq_str in list(cluster_sequences.keys()):
            cluster_sequences[seq_str].append(index)
            clusters_assigned[index] = cluster_indices[seq_str]

        # If the sequence does not exist, create a new cluster
        else:
            cluster_sequences[seq_str] = [index]
            cluster_indices[seq_str] = cluster_count
            clusters_assigned[index] = cluster_indices[seq_str]
            sequence_list.append(row[column])
            cluster_count += 1
    # reverse cluster-index dictionary for easier processing
    reversed_cluster_indices = dict(
        zip(list(cluster_indices.values()), list(cluster_indices.keys()))
    )

    return cluster_sequences, reversed_cluster_indices, clusters_assigned, sequence_list


def get_1snp_cluster(
    df, cluster_dict, clusters_diff_list, df_column="substitutions_list"
):
    """
    Compute the 1-SNP clusters. The clusters are not mutually exclusive.
    This method looks for the differences between each sequence and each cluster, and if the differences are <2 then they are added to the cluster.

    Args:
        df (pandas.DataFrame): table having the information to compute the difference from (substitutions).
        cluster_dict (dict): dictionary with the each 0-SNP cluster sequence as a string key, and the sequences belonging in the cluster as values.
        clusters_diff_list (list): list of the lists of substitutions of each cluster.
        df_column (str, optional): name of the dataframe column containing the information to compare. Defaults to "substitutions_list".

    Returns:
        dictionary: dictionary containing the 1-SNP clusters, it's the updated version of cluster_dict.
    """
    print("Getting 1-SNP clusters. \n")
    cluster_dict_temp = deepcopy(cluster_dict)
    # keys_dict = list(cluster_dict_temp.keys())

    values_to_iterate = df[df_column].values
    indices = df[df_column].index

    for index, row in enumerate(tqdm(values_to_iterate)):
        for i in range(len(clusters_diff_list)):
            # Convert key list from string to list
            differences = compute_snp_differences(row, clusters_diff_list[i])

            if differences < 2:
                if indices[index] not in cluster_dict_temp[str(clusters_diff_list[i])]:
                    cluster_dict_temp[str(clusters_diff_list[i])].append(indices[index])
    return cluster_dict_temp


def get_dendrogram_clusters(leaves, colors):
    """
    Get hierarchical clusters from dendrogram.

    Args:
        leaves (list): list of the leave values.
        colors (list): list containing the colors assigned to each leave.

    Returns:
        cluster_list: list containing the merged clusters ids.
        cluster_dict: dictionary containing the assignation of each 1-SNP cluster with each merged cluster.
    """
    cluster_count = 0
    prev_color = None
    next_color = None
    clusters_list = []
    clusters_dict = {}

    for i, color in enumerate(colors):
        if i == 0:
            clusters_list.append(cluster_count)
            clusters_dict[cluster_count] = []
            clusters_dict[cluster_count].append(int(leaves[i]))

        elif i > 0:
            prev_color = colors[i - 1]
            if i < len(colors) - 1:
                next_color = colors[i + 1]

            if (prev_color == color) and (color != "C0"):
                clusters_list.append(cluster_count)
                clusters_dict[cluster_count].append(int(leaves[i]))

            elif (prev_color == color) and (color == "C0"):
                cluster_count += 1
                clusters_list.append(cluster_count)
                clusters_dict[cluster_count] = []
                clusters_dict[cluster_count].append(int(leaves[i]))

            elif (i < len(colors)) and (prev_color == "C0") and (next_color != color):
                # Add the values for the current index to the previoust cluster
                clusters_list.append(cluster_count)
                clusters_dict[cluster_count].append(int(leaves[i]))

            elif prev_color != color:
                cluster_count += 1
                clusters_list.append(cluster_count)
                clusters_dict[cluster_count] = []
                clusters_dict[cluster_count].append(int(leaves[i]))

    return clusters_list, clusters_dict


def merge_clusters(seq_df, clusters_1_snp, cluster_info, threshold=1.0):
    """
    Get the merged clusters using hierarchical clustering.

    Args:
        seq_df (pandas.DataFrame): Dataframe containing all the sequences.
        clusters_1_snp (dict): Dictionary containing as keys the 1-SNP clusters ids, and as values the corresponding sequences.
        threshold (float, optional): Distance threshold to perform the hierarchical clustering. Defaults to 1.0.

    Returns:
        cluster_dict: dictionary containing the assignation of each 1-SNP cluster with each merged cluster.
        merged_seq: dictionary containing the merged_cluster_id: seq_list assignment.
        seq_cluster_dict: dictionary containing the seq_list: merged_cluster_id assignment.
        
    """

    seq_df_reset_index = seq_df.reset_index().rename(columns={"index": "prev_index"})
    remaining_clusters = seq_df.clusters.unique()
    seq_max = seq_df_reset_index.index.max()
    feature_dict = {}
    merged_seq = {}
    seq_new_to_old_index_dict = dict(
        zip(seq_df_reset_index.index, seq_df_reset_index.prev_index)
    )  # Track the equivalence between the new and the old sequence index
    print("Merging 1SNP clusters")
    print("Creating feature vectors ...")
    cluster_fvector_count = 0
    cluster_f_vector_original = {}
    for ind, (key, value) in enumerate(tqdm(clusters_1_snp.items())):
        if not ind in list(remaining_clusters):
            continue
        cluster_f_vector_original[cluster_fvector_count] = ind
        cluster_fvector_count += 1
        f_vector = np.zeros(seq_max + 1, dtype=bool)  # +1 because indices start with 0
        indices = list(
            seq_df_reset_index.loc[
                seq_df_reset_index.prev_index.isin(value)
            ].index.values
        )
        f_vector[indices] = True
        feature_dict[key] = f_vector

    print("Creating adjacency list ...")
    # Get the feature matrix given the feature vectors
    f_vector_array = np.vstack(list(feature_dict.values()))

    # Create adjacency matrix
    adjacency_list = []

    for i in tqdm(range(f_vector_array.shape[1])):
        column_true_values = np.where(f_vector_array[:, i])[0]

        if len(column_true_values) > 1:
            for j in range(1, len(column_true_values)):
                if (column_true_values[0], column_true_values[j]) in adjacency_list:
                    continue
                adjacency_list.append((column_true_values[0], column_true_values[j]))

    # c_grid = sns.clustermap(
    #    f_vector_array, metric="jaccard", col_cluster=True, row_cluster=True
    # )
    # c_grid.ax_col_dendrogram.set_title(
    #    "Clustermap showing the sequence overlap between 1SNP clusters"
    # )

    """

    # Get the thresholded clusters
    Z = scipy.cluster.hierarchy.linkage(f_vector_array, metric="jaccard")
    fig = plt.figure(figsize=(40,40))
    plt.title("Hierarchical clustering of the 1-SNP clusters")
    # dend_thresholded = scipy.cluster.hierarchy.dendrogram(
    #    c_grid.dendrogram_row.linkage, get_leaves=True, color_threshold=threshold
    # )
    dend_thresholded = scipy.cluster.hierarchy.dendrogram(
        Z, get_leaves=True, color_threshold=threshold
    )
    fig.savefig("clusters.png")
    # plt.show()
    plt.close()
    # Get the ordered leaves
    leaves = dend_thresholded["ivl"]
    # Get the assigned order
    colors = dend_thresholded["leaves_color_list"]

    cluster_list, cluster_dict = get_dendrogram_clusters(leaves, colors)
    
    """
    print("Merging clusters --> UnionFind")
    cluster_list, _, _ = create_disjoint_sets(
        adjacency_list, np.arange(0, f_vector_array.shape[0])
    )

    # Correct cluster indices to match 0 and 1-SNP clusters
    original_clusters = list(cluster_f_vector_original.values())
    cluster_list_corrected = [cluster_f_vector_original[c] for c in cluster_list]

    # Get merged cluster dictionary--> Get the assignation of the merged_cluster: 1SNP cluster
    print("Recap merged clusters...")
    merged_cluster_dict = {}
    for i in tqdm(range(len(cluster_list_corrected))):
        if cluster_list_corrected[i] in list(merged_cluster_dict.keys()):
            merged_cluster_dict[cluster_list_corrected[i]].append(original_clusters[i])
        else:
            merged_cluster_dict[cluster_list_corrected[i]] = [original_clusters[i]]

    # Merge clusters with sequences
    print("Creating merged clusters...")
    # clusters_keys = list(clusters_1_snp.keys())
    for key, value in merged_cluster_dict.items():
        merged_seq[key] = []
        for val in value:
            cluster_seq_id = cluster_info[1][val]
            merged_seq[key] = merged_seq[key] + clusters_1_snp[cluster_seq_id]

        merged_seq[key] = list(np.unique(np.array(merged_seq[key])))

    # Get reversed dictionary seg:cluster
    seq_cluster_dict = {}
    for cluster, sequences in merged_seq.items():
        for seq in sequences:
            seq_cluster_dict[seq] = cluster

    return merged_cluster_dict, merged_seq, seq_cluster_dict


# A class to represent a disjoint set
class UnionFind:
    def __init__(self):
        self.parent = {}
        self.rank = {}

    # perform MakeSet operation
    def makeSet(self, universe):

        # create `n` disjoint sets (one for each item)
        for i in universe:
            self.parent[i] = None
            self.rank[i] = 0

    # Find the root of the set in which element `k` belongs
    def find(self, k):

        # if `k` is root
        if self.parent[k] == None:
            return k

        # recursion for the parent until we find the root
        return self.find(self.parent[k])

    # Perform Union of two subsets
    def union(self, a, b):

        # find the root of the sets in which elements
        # `x` and `y` belongs
        x = self.find(a)
        y = self.find(b)

        if x != y:
            if self.rank[x] < self.rank[y]:
                self.parent[x] = y
            elif self.rank[y] < self.rank[x]:
                self.parent[y] = x
            elif self.rank[x] == self.rank[y]:
                self.parent[y] = x
                self.rank[x] = self.rank[x] + 1

    def get_assignation(self, indices):
        return [self.find(i) for i in indices]


def create_disjoint_sets(adj_list, indices):
    uf = UnionFind()
    uf.makeSet(indices)
    for tup in tqdm(adj_list):

        if tup[0] == tup[1]:
            continue
        uf.union(tup[0], tup[1])

    asigned_clusters = uf.get_assignation(indices)
    return asigned_clusters, uf.parent, uf.rank
