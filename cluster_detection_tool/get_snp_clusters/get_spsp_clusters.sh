#!/bin/bash
#SBATCH --export=NONE
#SBATCH --mem=50G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --job-name=spsp_get_clusters
#SBATCH --error=spsps_clusters_error-%j.log
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=blanca.cabreragil@sib.swiss


module load python
conda activate gisaid_clusters
python /home/spsp/data/NG/spsp-tools/cluster-tool/get_clusters.py