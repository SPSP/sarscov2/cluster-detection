"""
This scripts compares a bunch of sequences and finds the ones with 0-SNP and 1-SNP differences. The inputs to the script are:
- LOCATION: name of the canton code to filter by only the sequences assign to this canton.
- CLUSTER_SIZE_LIMIT: Maximum and minimum sizes of SNP clusters to retrieve. Maximum and minimum vales are included.
- DAYS_SPAN_LIMIT: Maximum and minimum number of days between the first detection of the cluster and the last one. Maximum and minimum vales are included.
- DATE_SPAN_LIMIT: Date window where to get the sequences from. Maximum and minimum vales are included.
- OUTPUT_DIRECTORY: Directory where to store the output files.
- ANALYSIS_NAME: Name of the current analysis. The output will be stored in a folder with this name.

- PANGOLIN_FILE: Path to the directory containing the pangolin file to use. If NONE the latest one from our archive will be loaded.
- NEXTCLADE_FILE: Path to the directory containing the nextclade file to use. If NONE the latest one from our archive will be loaded.
- SAMPLES_METADATA_FILE: Path to the directory containing the samples metadata file to use. If NONE the latest one from our archive will be loaded.

- SEQUENCES_TO_QUERY: Path to the file or directory containing the information about the sequences to query.
- METADATA_TO_QUERY: Path to the .tsv file containing the data samples. This file can only be a .tsv and must with columns strain_name, isolation_date, location_general. It is very important that the strain_name is the same as in the sequences to query.
- MERGE_CLUSTERS: Merge the 1-SNP clusters using hierarchical clustering. Set to true or false according to your needs.
- HC_THRESHOLD: Apply a user-definde threshold on  the hierarchical clustering. This values should range from 0-1. The default value is 1.
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from datetime import datetime
from copy import deepcopy
import matplotlib
import time
import os
import glob
import ast
import shutil
import yaml
import sys
import logging
import json


sys.setrecursionlimit(2500)

from get_snp_clusters.utils import (
    assert_input_str_list,
    read_files,
    filter_by_cluster_size,
    filter_by_days,
    filter_by_location,
    filter_by_date,
    get_cluster_df,
    plot_lineage_counts,
    plot_lineage_data,
    draw_tree,
    assert_input_int_list,
    assert_input_str_list,
    create_multifasta,
    execute_nextclade,
    execute_pangolin,
    merge_string,
)
from get_snp_clusters.clustering_methods import (
    get_0snp_clusters,
    get_1snp_cluster,
    add_clusters_to_df,
    merge_clusters,
    create_disjoint_sets,
    MinimumSpanningTree,
)


def get_snp_clusters(inputs):
    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    # Global variables
    LOCATION = inputs["LOCATION"]
    # Cluster size window
    CLUSTER_SIZE_LIMIT = inputs["CLUSTER_SIZE_LIMIT"]
    # Number of days window
    DAYS_SPAN_LIMIT = inputs["DAYS_SPAN_LIMIT"]
    # Date window
    DATE_SPAN_LIMIT = inputs["DATE_SPAN_LIMIT"]
    # Output directory where to store the output data
    OUTPUT_DIRECTORY = inputs["OUTPUT_DIRECTORY"]
    # Name of the current analysis
    ANALYSIS_NAME = inputs["ANALYSIS_NAME"]

    ##### Sequences (If none, the files from the archive will be loaded) #####
    PANGOLIN_FILE = inputs["PANGOLIN_FILE"]
    NEXTCLADE_FILE = inputs["NEXTCLADE_FILE"]
    SAMPLES_METADATA_FILE = inputs["SAMPLES_METADATA_FILE"]
    ############################################################################

    # If 1SNP clusters should be merged
    MERGE_CLUSTERS = inputs["MERGE_CLUSTERS"]
    # Hierarchical clustering threshold
    HC_THRESHOLD = 1

    # Query sequences information
    DIRECTORY_OF_THE_SEQUENCES_TO_QUERY = inputs["SEQUENCES_TO_QUERY"]
    DIRECTORY_OF_THE_METADATA_TO_QUERY = inputs["METADATA_TO_QUERY"]
    FASTA_PATTERN = inputs["FASTA_PATTERN"]

    # Log scale for scatterplot x axis
    LOG_SCALE = inputs["LOG_SCALE"]

    SAMPLES = inputs["SAMPLES"]

    # Set cluster size limits
    if CLUSTER_SIZE_LIMIT:
        assert_input_int_list(CLUSTER_SIZE_LIMIT, "CLUSTER_SIZE_LIMIT")
        cluster_size_upper_limit = max(CLUSTER_SIZE_LIMIT)
        cluster_size_lower_limit = min(CLUSTER_SIZE_LIMIT)
    else:
        cluster_size_upper_limit = 1000000000 * 100000000
        cluster_size_lower_limit = 0

    # Set days window limit
    if DAYS_SPAN_LIMIT:
        assert_input_int_list(DAYS_SPAN_LIMIT, "DAYS_SPAN_LIMIT")
        days_span_upper_limit = max(DAYS_SPAN_LIMIT)
        days_span_lower_limit = min(DAYS_SPAN_LIMIT)
    else:
        days_span_upper_limit = 1000000000 * 100000000
        days_span_lower_limit = 0

    # Set date span limits
    if DATE_SPAN_LIMIT:
        assert_input_str_list(DATE_SPAN_LIMIT, "DATE_SPAN_LIMIT")
        date_span_upper_limit = datetime.strptime(DATE_SPAN_LIMIT[1], "%Y-%m-%d")
        date_span_lower_limit = datetime.strptime(DATE_SPAN_LIMIT[0], "%Y-%m-%d")
    else:
        date_span_upper_limit = datetime.strptime("2050-12-31", "%Y-%m-%d")
        date_span_lower_limit = datetime.strptime("1900-01-01", "%Y-%m-%d")

    # Plot legend
    # plot_mst_tree = False # MST not active
    plot_group_by = (
        "lineage"  # How to group the classes in the legend: "lineage" or "cluster"
    )

    # Set output file path
    if OUTPUT_DIRECTORY:
        assert isinstance(OUTPUT_DIRECTORY, str), "OUTPUT_DIRECTORY must be a string."
        output_directory = OUTPUT_DIRECTORY

    else:
        output_directory = os.getcwd()  # Path were to store the output datafiles

    # Assign output file names
    if ANALYSIS_NAME:
        assert isinstance(ANALYSIS_NAME, str), "ANALYSIS_NAME must be a string."
        directory_name = ANALYSIS_NAME
    else:
        directory_name = datetime.now().strftime("%Y%m%d-%H%M%S")

    output_path = os.path.join(output_directory, directory_name)

    # Create the main output directory in case it does not exist
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    # init the logger
    logging.basicConfig(
        filename=os.path.join(output_path, "cluster_detection.log"),
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        force=True,
    )
    logging.info("Execution started...")

    # Cluster algorithm for >1SNP (MST not active)
    # mst_algorithm = "kruskal"  # Minimum Spanning Tree algorithm: "kruskal" or "prim"

    # Set input files
    if PANGOLIN_FILE:
        assert isinstance(PANGOLIN_FILE, str), "PANGOLIN_FILE must be a string."
        pangolin_file = PANGOLIN_FILE
    else:
        pangolin_file = glob.glob("latest_ch_data/*_PANGOLIN.csv")[0]

    if NEXTCLADE_FILE:
        assert isinstance(NEXTCLADE_FILE, str), "NEXTCLADE_FILE must be a string."
        nextclade_file = NEXTCLADE_FILE
    else:
        nextclade_file = glob.glob("latest_ch_data/*_NEXTCLADE.tsv")[0]

    real_datafile = True
    if SAMPLES_METADATA_FILE:
        assert isinstance(
            SAMPLES_METADATA_FILE, str
        ), "SAMPLES_METADATA_FILE must be a string."
        metadata_file = SAMPLES_METADATA_FILE
    else:
        metadata_file = glob.glob("latest_ch_data/samples_*9.tsv")[0]

    # If there are sequences to query check if it is a multifasta file, in negative case, create the multifasta file
    if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
        # Get the multifasta file to process with nextclade
        if FASTA_PATTERN.replace("*", "") not in DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:

            try:
                multifasta_directory = create_multifasta(
                    DIRECTORY_OF_THE_SEQUENCES_TO_QUERY, FASTA_PATTERN
                )
            except RuntimeError as e:
                print(
                    "An error occurred during the creation of the multifasta file: \n %s"
                    % e
                )

        else:
            multifasta_directory = DIRECTORY_OF_THE_SEQUENCES_TO_QUERY

        # Obtain the nextclade substitutions for the queried sequences
        print("Running nextclade ...")
        logging.info("Running nextclade. \n")
        try:
            execute_nextclade(multifasta_directory, output_path)
        except RuntimeError as err:
            print("An error occurred during the execution of nextclade: %s" % err)

        print("Running pangolin ...")
        logging.info("Running pangolin. \n")
        try:
            execute_pangolin(multifasta_directory)
            shutil.move(
                "lineage_report.csv", os.path.join(output_path, "lineage_report.csv")
            )
        except RuntimeError as err:
            print("An error occurred during the execution of pangolin: %s" % err)

        # Create query dataframe
        nextclade_df = pd.read_csv(
            os.path.join(output_path, "output_nextclade", "nextclade.tsv"), sep="\t"
        )
        query_df = nextclade_df[
            [
                "sample.strain_name",
                "assembly.annotation_next_clade.substitutions",
                "assembly.annotation_next_clade.aa_substitutions",
            ]
        ]

        # Load pangolin results
        pangolin_df = pd.read_csv(os.path.join(output_path, "lineage_report.csv"))
        temp_df = pangolin_df[
            ["sample.strain_name", "assembly.annotation_pangolin.lineage"]
        ]  # .rename(columns={"taxon": "seqName"})

        # Merge nextclade and pangolin information
        query_df = query_df.merge(temp_df, on="sample.strain_name")

        # Create the substitutions in list format
        query_df["substitutions_list"] = query_df.apply(
            lambda row: sorted(
                row["assembly.annotation_next_clade.substitutions"].split(",")
            )
            if isinstance(row["assembly.annotation_next_clade.substitutions"], str)
            else [],
            axis=1,
        )
        # Add an identifyer for query sequences
        query_df["query"] = 1

        # Add samples metadata if available
        if DIRECTORY_OF_THE_METADATA_TO_QUERY:
            query_temp = query_df.copy()
            query_metadata = pd.read_csv(
                DIRECTORY_OF_THE_METADATA_TO_QUERY, sep="\t"
            )  # load a tsv file with metadata samples
            # query_metadata = query_metadata.rename(columns={"strain_name": "seqName"})
            query_metadata["sample.isolation_date"] = pd.to_datetime(
                query_metadata["sample.isolation_date"]
            )

            # hack
            query_metadata["sample.strain_name"] = query_metadata.apply(
                lambda row: "hCoV-19/" + row.seqName, axis=1
            )  # Uncomment in case seqName do

            len_df = len(query_temp)
            len_metadata = len(query_metadata)

            query_df = query_temp.merge(
                query_metadata[
                    [
                        "sample.strain_name",
                        "sample.isolation_date",
                        "sample_origin_location.country_division",
                    ]
                ],
                on="sample.strain_name",
            )
            len_merged = len(query_df)
            logging.info(
                "During the metadata merge we lost {} values from the query data and {} from the metadata. \n".format(
                    len_df - len_merged, len_metadata - len_merged
                )
            )

        # Get the sequence names for the queried sequences
        query_names = list(query_df["sample.strain_name"].values)

    # Plot config
    cmap = "viridis"  # Tree plot colormap
    max_lineages = 10  # Maximum lineages to show in the analysis.
    add_legend = True  # Add legend to the plot. For labs with lots of classes this can take some time

    # Set location name
    if LOCATION:
        assert isinstance(
            LOCATION, list
        ), "LOCATION must be a list of the selected locations."
        location_meta = LOCATION
    else:
        location_meta = "all"

    # Create 0 & 1 snp clusters directory name
    clusters_path = os.path.join(
        output_path,
        "clusters_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}".format(
            cluster_size_lower_limit,
            cluster_size_upper_limit,
            date_span_lower_limit.strftime("%Y-%m-%d"),
            date_span_upper_limit.strftime("%Y-%m-%d"),
            days_span_lower_limit,
            days_span_upper_limit,
            merge_string(location_meta),
        ),
    )

    # Create merged clusters directory name
    if MERGE_CLUSTERS:
        merged_clusters_path = os.path.join(
            output_path,
            "merged_clusters_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}".format(
                cluster_size_lower_limit,
                cluster_size_upper_limit,
                date_span_lower_limit.strftime("%Y-%m-%d"),
                date_span_upper_limit.strftime("%Y-%m-%d"),
                days_span_lower_limit,
                days_span_upper_limit,
                merge_string(location_meta),
            ),
        )

    # Create the clusters directory in case it does not exist
    if not os.path.exists(clusters_path):
        os.mkdir(clusters_path)

    # Create the merged clusters directory in cas it does not exist
    if MERGE_CLUSTERS:
        if not os.path.exists(merged_clusters_path):
            os.mkdir(merged_clusters_path)

    # Load data
    df, metadata_df = read_files(
        pangolin_file, nextclade_file, metadata_file, real_datafile=real_datafile
    )
    len_df = len(df)
    len_metadata = len(metadata_df)

    merged_df = df.merge(
        metadata_df[
            [
                "sample.strain_name",
                "sample.isolation_date",
                "sample_origin_location.country_division",
            ]
        ],
        on="sample.strain_name",
    )
    len_merged = len(merged_df)
    logging.info(
        "During the merge we lost {} values from df and {} from the metadata. \n".format(
            len_df - len_merged, len_metadata - len_merged
        )
    )
    if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
        # Remove sequences from base file that are in the queried ones
        logging.info("There are %i sequences to be queried. \n" % len(query_names))
        merged_df = merged_df.drop(
            merged_df.loc[merged_df.seqName.isin(query_names)].index
        )
        logging.info(
            "{} sequences have been removed as they are to be queried. \n".format(
                len_merged - len(merged_df)
            )
        )
    # GET SAMPLES

    if isinstance(SAMPLES, int):
        merged_df = merged_df.sample(SAMPLES)

        print("Sampling dataset, len: ", len(merged_df))

    # Filter dataframes on location
    if LOCATION:
        merged_df, not_in_location = filter_by_location(merged_df, LOCATION)
        not_in_location.to_csv(os.path.join(output_path, "filtered_by_location.csv"))
        len_loc_merge = len(merged_df)
        logging.info(
            "When filtering by location {} entries were dropped, and {} remained. \n".format(
                len_merged - len_loc_merge, len_loc_merge
            )
        )
        if len_loc_merge < 1:
            raise Exception("No sequences left after filtering by location!")

    # Filter dataframe based on date window
    if DATE_SPAN_LIMIT:
        len_before_date_filter = len(merged_df)
        merged_df, not_in_date = filter_by_date(
            merged_df, date_span_upper_limit, date_span_lower_limit
        )
        print("len not_in_date: ", len(not_in_date))
        not_in_date.to_csv(os.path.join(output_path, "filtered_by_date.csv"))
        
        
        len_after_date_filter = len(merged_df)
        logging.info(
            "When filtering by date {} entries were dropped, and {} remained. \n".format(
                len_before_date_filter - len_after_date_filter, len_after_date_filter
            )
        )

        if len_after_date_filter < 1:
            raise Exception("No sequences left after filtering by date span limit!")

    merged_df["query"] = 0
    if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
        merged_df = pd.concat([merged_df, query_df], ignore_index=True)

    if os.path.isfile(
        os.path.join(output_path, "zero_snp_filtered_sequences.csv")
    ) and os.path.isfile(os.path.join(output_path, "cluster_info.json")):
        df_clusters = pd.read_csv(
            os.path.join(output_path, "zero_snp_filtered_sequences.csv"), index_col=0,
        )

        with open(os.path.join(output_path, "cluster_info.json")) as file:
            cluster_info = json.load(file)
            cluster_info[1] = {int(k): v for k, v in cluster_info[1].items()}

    else:
        # 0SNP clusters
        cluster_info = get_0snp_clusters(merged_df, column="substitutions_list")

        df_clusters = add_clusters_to_df(
            merged_df, cluster_info[2]  # dict(sorted(cluster_info[2].items()))
        )
        
        df_clusters.to_csv(os.path.join(output_path, "0SNP_clusters.csv"))

        # Filter dataframes on cluster size
        if CLUSTER_SIZE_LIMIT:
            df_clusters, removed_clusters_size = filter_by_cluster_size(
                df_clusters, cluster_size_upper_limit, cluster_size_lower_limit
            )
            removed_clusters_size.to_csv(os.path.join(output_path, "filtered_by_cluster_size.csv"))
            len_clusters = len(df_clusters)
            logging.info(
                "When filtering by cluster size {} entries were dropped, and {} remained. \n".format(
                    len(merged_df) - len_clusters, len_clusters
                )
            )
            if len_clusters < 1:
                raise Exception("No sequences left after filtering by cluster size!")

        # Filter dataframes on days span
        if DAYS_SPAN_LIMIT:
            init_len = len(df_clusters)
            df_clusters, larger_than_span = filter_by_days(
                df_clusters, days_span_upper_limit, days_span_lower_limit
            )
            larger_than_span.to_csv(os.path.join(output_path, "filtered_by_days_span.csv"))
            len_date = len(df_clusters)
            logging.info(
                "When filtering by days {} entries were dropped, and {} remained. \n".format(
                    init_len - len_date, len_date
                )
            )

            if len_date < 1:
                raise Exception(
                    "No sequences left after filtering by number of days within the cluster!"
                )

        df_clusters.to_csv(os.path.join(output_path, "zero_snp_filtered_sequences.csv"))
        with open(os.path.join(output_path, "cluster_info.json"), "w") as file:
            json.dump(cluster_info, file)

    # Compute 1SNP clusters
    if os.path.isfile(
        os.path.join(output_path, "1_snp_potential_outbreaks_summary.csv")
    ):
        potential_outbreaks_summary = pd.read_csv(
            os.path.join(output_path, "1_snp_potential_outbreaks_summary.csv"),
            index_col=0,
        )
        with open(os.path.join(output_path, "clusters_1snp.json")) as file:
            clusters_1snp = json.load(file)
            clusters_1snp = {
                k: list(map(int, v.strip("[]").split(",")))
                for k, v in clusters_1snp.items()
            }
    else:
        clusters_dict = deepcopy(cluster_info[0])
        clusters_diff_list = cluster_info[3]
        start = time.time()
        clusters_1snp = get_1snp_cluster(
            df_clusters,
            clusters_dict,
            clusters_diff_list,
            df_column="substitutions_list",
        )
        print("It took {} s to get the 1-SNP clusters. \n".format(time.time() - start))

        # Save cluster dataset
        potential_outbreaks_summary = get_cluster_df(
            df_clusters, clusters_1snp, cluster_info
        )

        potential_outbreaks_summary.to_csv(
            os.path.join(output_path, "1_snp_potential_outbreaks_summary.csv")
        )

        with open(os.path.join(output_path, "clusters_1snp.json"), "w") as file:
            clusters_1snp_str = {
                key: str(value) for key, value in clusters_1snp.items()
            }
            json.dump(clusters_1snp_str, file)

    if os.path.isfile(
        os.path.join(output_path, "merged_potential_outbreaks_summary.csv")
    ) and os.path.isfile((os.path.join(output_path, "merged_seq_clusters.json"))):

        potential_outbreaks_summary = pd.read_csv(
            os.path.join(output_path, "merged_potential_outbreaks_summary.csv"),
            index_col=0,
        )

        with open(os.path.join(output_path, "merged_seq_clusters.json")) as file:
            merged_seq_clusters.load()

    else:
        # Merge 1SNP clusters
        if MERGE_CLUSTERS:

            # Create output dataframe
            final_dataframe = df_clusters[
                [
                    "sample.strain_name",
                    "assembly.annotation_pangolin.lineage",
                    "clusters",
                    "sample.isolation_date",
                    "sample_origin_location.country_division",
                    "query",
                ]
            ]

            # Merge 1snp clusters
            if HC_THRESHOLD:
                threshold = HC_THRESHOLD
            else:
                threshold = 1.0
            (
                merged_clusters_1snp_clusters,
                merged_clusters,
                merged_seq_clusters,
            ) = merge_clusters(final_dataframe, clusters_1snp, cluster_info, threshold)

            potential_outbreaks_summary["meta_clusters"] = -1

            # Assign the merged cluster id for each 0SNP cluster
            for merged, snp in merged_clusters_1snp_clusters.items():
                for s in snp:
                    potential_outbreaks_summary.loc[
                        potential_outbreaks_summary.cluster_id == s, "meta_clusters"
                    ] = merged

            # Add merged clusters to potential_outbreaks_summary
            potential_outbreaks_summary = potential_outbreaks_summary.sort_values(
                by=["meta_clusters", "lineage(0)", "cluster_id"], ascending=True
            )[
                [
                    "meta_clusters",
                    "cluster_id",
                    "num_cases(0)",
                    "lineage(0)",
                    "start_date(0)",
                    "end_date(0)",
                    "cantons(0)",
                    "query(0)",
                    "num_cases(1)",
                    "lineage(1)",
                    "start_date(1)",
                    "end_date(1)",
                    "cantons(1)",
                    "query(1)",
                ]
            ].reset_index(
                drop=True
            )
        else:
            potential_outbreaks_summary = potential_outbreaks_summary.sort_values(
                by=["lineage(0)", "cluster_id"], ascending=True
            ).reset_index(drop=True)

            with open(os.path.join(output_path, "merged_seq_clusters.json")) as file:
                json.dump(merged_seq_clusters, file)

        potential_outbreaks_summary.to_csv(
            os.path.join(output_path, "merged_potential_outbreaks_summary.csv")
        )

    # Create final_dataframe with merge_clusters if true or without if false
    if MERGE_CLUSTERS:
        merged_dataframe = pd.DataFrame()
        merged_dataframe["index_old"] = list(merged_seq_clusters.keys())
        merged_dataframe["merged_clusters"] = list(merged_seq_clusters.values())
        merged_dataframe = merged_dataframe.reset_index()
        merged_len = len(merged_dataframe)
        final_dataframe = final_dataframe.reset_index()
        final_dataframe = final_dataframe.rename(columns={"index": "index_old"})
        total_seq = len(final_dataframe)
        final_dataframe = final_dataframe.merge(merged_dataframe, on="index_old")
        print(
            "When merging the merged dataframe with the total sequences {} where lost from the merged dataframe, and {} from the total sequences.".format(
                merged_len - len(final_dataframe), total_seq - len(final_dataframe)
            )
        )
        final_dataframe = final_dataframe.rename(columns={"index_old": "index"})
        final_dataframe = final_dataframe.sort_values(
            by=["assembly.annotation_pangolin.lineage", "merged_clusters"],
            ascending=True,
        ).drop(columns=["index"])

    else:
        # Create output dataframe
        final_dataframe = df_clusters[
            [
                "sample.strain_name",
                "assembly.annotation_pangolin.lineage",
                "clusters",
                "sample.isolation_date",
                "sample_origin_location.country_division",
                "query",
            ]
        ]
        # Sort values and reset index to final_dataframe
        final_dataframe = final_dataframe.sort_values(
            by=["assembly.annotation_pangolin.lineage", "clusters"], ascending=True
        ).reset_index(drop=True)

    # In case there are sequences to query retrieve only the information from the data belonging to those clusters
    if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
        if MERGE_CLUSTERS:
            seq_clusters = list(
                final_dataframe.loc[
                    final_dataframe["sample.strain_name"].isin(
                        query_df["sample.strain_name"].values
                    )
                ].merged_clusters.values
            )
            potential_outbreaks_summary = potential_outbreaks_summary.loc[
                potential_outbreaks_summary.meta_clusters.isin(seq_clusters)
            ]

        else:
            seq_clusters = list(
                df_clusters.loc[
                    df_clusters["sample.strain_name"].isin(
                        query_df["sample.strain_name"].values
                    )
                ].clusters.values
            )
            potential_outbreaks_summary = potential_outbreaks_summary.loc[
                potential_outbreaks_summary.cluster_id.isin(seq_clusters)
            ]

    save_name_outbreaks = "potential_outbreaks_summary_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}.tsv".format(
        cluster_size_lower_limit,
        cluster_size_upper_limit,
        date_span_lower_limit.strftime("%Y-%m-%d"),
        date_span_upper_limit.strftime("%Y-%m-%d"),
        days_span_lower_limit,
        days_span_upper_limit,
        merge_string(location_meta),
    )

    potential_outbreaks_summary.to_csv(
        os.path.join(output_path, save_name_outbreaks,), sep="\t", index=False,
    )
    print(
        "Potential outpreaks summary saved in {}. \n".format(
            os.path.join(output_path, save_name_outbreaks,)
        )
    )

    save_name_all_sequences = "potential_outbreaks_summary_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}_all_sequences.tsv".format(
        cluster_size_lower_limit,
        cluster_size_upper_limit,
        date_span_lower_limit.strftime("%Y-%m-%d"),
        date_span_upper_limit.strftime("%Y-%m-%d"),
        days_span_lower_limit,
        days_span_upper_limit,
        merge_string(location_meta),
    )

    # In case there are sequences to query retrieve only the information from the data belonging to those clusters
    if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
        final_dataframe_snp = final_dataframe.loc[
            final_dataframe.clusters.isin(seq_clusters)
        ]
        final_dataframe_snp.to_csv(
            os.path.join(
                output_path,
                save_name_all_sequences.split(".")[0]
                + "_snp."
                + save_name_all_sequences.split(".")[1],
            ),
            sep="\t",
            index=False,
        )

        if MERGE_CLUSTERS:
            seq_merged_clusters = list(
                final_dataframe.loc[
                    final_dataframe["sample.strain_name"].isin(
                        query_df["sample.strain_name"].values
                    )
                ].merged_clusters.values
            )

            final_dataframe_merged = final_dataframe.loc[
                final_dataframe.merged_clusters.isin(seq_merged_clusters)
            ]
            final_dataframe_merged.to_csv(
                os.path.join(
                    output_path,
                    save_name_all_sequences.split(".")[0]
                    + "_merged."
                    + save_name_all_sequences.split(".")[1],
                ),
                sep="\t",
                index=False,
            )

    else:
        final_dataframe.to_csv(
            os.path.join(output_path, save_name_all_sequences,), sep="\t", index=False,
        )
    print(
        "Potential outpreaks all sequences saved in {}. \n".format(
            os.path.join(output_path, save_name_all_sequences,)
        )
    )

    # Save per-cluster information
    for cluster in df_clusters.clusters.unique():
        if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
            if cluster not in seq_clusters:
                continue
        # Get sequences at 0SNP distance
        snp0_indices = cluster_info[0][cluster_info[1][cluster]]
        cluster_temp_df = df_clusters.loc[df_clusters.index.isin(snp0_indices)][
            [
                "assembly.annotation_pangolin.lineage",
                "sample.strain_name",
                "sample.isolation_date",
                "sample_origin_location.country_division",
                "query",
            ]
        ]
        cluster_temp_df["SNP_distance"] = 0

        # Get sequences at 1SNP distance
        snp1_indices = clusters_1snp[cluster_info[1][cluster]]

        # Get the 1SNP sequences
        snp1_indices_not_in_snp0 = list(set(snp0_indices) ^ set(snp1_indices))

        # Add the sequences to a second dataframe
        cluster_temp_df_2 = df_clusters.loc[snp1_indices_not_in_snp0][
            [
                "assembly.annotation_pangolin.lineage",
                "sample.strain_name",
                "sample.isolation_date",
                "sample_origin_location.country_division",
                "query",
            ]
        ]
        cluster_temp_df_2["SNP_distance"] = 1

        # Concatenate both dataframes
        cluster_i_df = pd.concat([cluster_temp_df, cluster_temp_df_2]).reset_index(
            drop=True
        )

        # Save cluster summary
        save_name_clusters = "potential_outbreaks_summary_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}_CL_{}.tsv".format(
            cluster_size_lower_limit,
            cluster_size_upper_limit,
            date_span_lower_limit.strftime("%Y-%m-%d"),
            date_span_upper_limit.strftime("%Y-%m-%d"),
            days_span_lower_limit,
            days_span_upper_limit,
            merge_string(location_meta),
            cluster,
        )
        cluster_i_df.to_csv(
            os.path.join(clusters_path, save_name_clusters,), sep="\t", index=False,
        )
    print(
        "Potential outpreaks summary per cluster saved in {}. \n".format(clusters_path),
    )

    # Save per-cluster information for the merged clusters
    if MERGE_CLUSTERS:

        # Save per-mergedcluster info
        for cl, seqs in merged_clusters.items():
            if DIRECTORY_OF_THE_SEQUENCES_TO_QUERY:
                if cl not in seq_merged_clusters:
                    continue

            cluster_temp = df_clusters.loc[df_clusters.index.isin(seqs)][
                [
                    "assembly.annotation_pangolin.lineage",
                    "sample.strain_name",
                    "sample.isolation_date",
                    "sample_origin_location.country_division",
                    "query",
                ]
            ]

            # Save cluster summary
            save_name_clusters = "potential_outbreaks_summary_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}_mCL_{}.tsv".format(
                cluster_size_lower_limit,
                cluster_size_upper_limit,
                date_span_lower_limit.strftime("%Y-%m-%d"),
                date_span_upper_limit.strftime("%Y-%m-%d"),
                days_span_lower_limit,
                days_span_upper_limit,
                merge_string(location_meta),
                cl,
            )

            cluster_temp.to_csv(
                os.path.join(merged_clusters_path, save_name_clusters,),
                sep="\t",
                index=False,
            )
        print(
            "Potential outpreaks summary per cluster saved in {}. \n".format(
                merged_clusters_path
            ),
        )

    """
    # Draw MST
    if plot_mst_tree:
        # Build Minimum Spanning Tree
        print("Building tree... \n")
        start = time.time()
        df_tree = df_clusters[
            ["substitutions", "substitutions_list", "lineage"]
        ].drop_duplicates(subset="substitutions")
        tree = MinimumSpanningTree(
            df_tree, mst_algorithm=mst_algorithm, column_to_compare="substitutions_list"
        )
        print("The tree has been built in {}s. \n".format(time.time() - start))
        print("Drawing tree... \n")
        start = time.time()

        cm_colors = plt.get_cmap(cmap).colors
        len_cmap = len(cm_colors)
        len_lineages = len(list(tree.lineage_categories.keys()))
        cmap_step = len_cmap / (len_lineages + 1)

        list_colors = []
        for i in range(len_lineages + 1):
            list_colors.append(cm_colors[i * int(cmap_step)])
        lineage_cmap = LinearSegmentedColormap.from_list(
            "lineage_cmap", list_colors, N=len(list_colors)
        )

        draw_tree(
            tree,
            cluster_info,
            group_by=plot_group_by,
            cmap=lineage_cmap,
            legend=add_legend,
        )
        print("The tree has been plotted in {}s. \n".format(time.time() - start))

    """
    # Plot cluster information
    fig = plt.figure(figsize=(13, 8))
    # plot_lineage_data(final_dataframe, max_lineages=max_lineages)
    fig.tight_layout(pad=3.0)
    # plt.show()
    fig.savefig(
        os.path.join(
            output_path,
            "cluster_lineage_barplots_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}.png".format(
                cluster_size_lower_limit,
                cluster_size_upper_limit,
                date_span_lower_limit.strftime("%Y-%m-%d"),
                date_span_upper_limit.strftime("%Y-%m-%d"),
                days_span_lower_limit,
                days_span_upper_limit,
                merge_string(location_meta),
            ),
        )
    )
    plt.close()

    mean_per_cluster = potential_outbreaks_summary.groupby(["lineage(0)"]).mean()
    mean_clusters = potential_outbreaks_summary.groupby(["lineage(0)"]).size().values

    mean_per_cluster["cluster_counts"] = mean_clusters + np.random.uniform(
        low=0, high=0.5, size=(len(mean_clusters),)
    )
    y_jitter = np.random.uniform(
        low=0, high=0.5, size=(len(mean_clusters),)
    )  # Add random jitter to avoid overlaps in the plot
    mean_per_cluster.reset_index(inplace=True)
    mean_per_cluster["num_cases(0)"] = mean_per_cluster["num_cases(0)"] + y_jitter

    mean_per_cluster["num_cases(1)"] = mean_per_cluster["num_cases(1)"] + y_jitter

    # Plot lineage counts 0-SNP/1-SNP
    fig, ax = plt.subplots(figsize=(15, 8))
    plot_lineage_counts(
        mean_per_cluster, "cluster_counts", "num_cases(0)", "lineage(0)"
    )

    # Uncomment to plot 1-SNP
    # plot_lineage_counts(
    #    mean_per_cluster, "cluster_counts", "num_cases(1)", "lineage(0)"
    # )
    # print(len(mean_per_cluster))

    dy = (
        mean_per_cluster["num_cases(1)"].values
        - mean_per_cluster["num_cases(0)"].values
    )
    x = mean_per_cluster["cluster_counts"].values
    y = mean_per_cluster["num_cases(0)"].values
    for i in range(len(dy)):
        matplotlib.pyplot.arrow(
            x[i], y[i], dx=0, dy=dy[i],
        )
    if LOG_SCALE:
        ax.set_xscale("log")
    # plt.show()
    fig.savefig(
        os.path.join(
            output_path,
            "cluster_lineage_scatter_NC_{}-{}_D_{}-{}_TW_{}-{}_LOC_{}.png".format(
                cluster_size_lower_limit,
                cluster_size_upper_limit,
                date_span_lower_limit.strftime("%Y-%m-%d"),
                date_span_upper_limit.strftime("%Y-%m-%d"),
                days_span_lower_limit,
                days_span_upper_limit,
                merge_string(location_meta),
            ),
        )
    )
    plt.close()


def main():
    print(os.getcwd())
    # Open input parameters file
    with open(
        "inputs.yaml"
    ) as file:  # with open(r"/home/spsp/data/NG/spsp-tools/cluster-tool/inputs.yaml") as file:
        inputs = yaml.full_load(file)

    get_snp_clusters(inputs)


if __name__ == "__main__":
    main()
