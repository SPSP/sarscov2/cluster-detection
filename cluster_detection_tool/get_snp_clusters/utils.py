import pylab
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import seaborn as sns
import subprocess
import pandas as pd
import pathlib
import os
from sklearn.metrics import jaccard_score
from tqdm import tqdm


PLOT_MULTIPLIER = 2


def assert_input_int_list(input_list, variable):
    assert len(input_list) == 2, (
        "The length of the list in %s must be of length 2, this means that it can only contain two values: lower and upper limit. Example: [lower, upper] "
        % variable
    )
    assert isinstance(input_list[0], int), (
        "Input %s must be a list of integer values" % variable
    )
    assert isinstance(input_list[1], int), (
        "Input %s must be a list of integer values" % variable
    )


def assert_input_str_list(input_list, variable):
    assert len(input_list) == 2, (
        "The length of the list in %s must be of length 2, this means that it can only contain two values: lower and upper limit. Example: [lower, upper] "
        % variable
    )
    assert isinstance(input_list[0], str), (
        "Input %s must be a list of string values in YYYY-MM-DD format" % variable
    )
    assert isinstance(input_list[1], str), (
        "Input %s must be a list of string values in YYYY-MM-DD format" % variable
    )


def create_mock_dataset(mock_df, df):
    """
    Creates a mock dataset with the information from mock_df
    :param mock_df: pandas.DataFrame containing the information
    :param df: pandas.DataFrame whith the same seqName values sas mock_df, the two dataframes will be merged
    on the this column
    :return: pandas.DataFrame containing seqName, strain_name, isolation_date and location
    """
    unique_strain_names = list(mock_df.strain_name.unique())
    unique_isolation_date = list(mock_df.isolation_date.unique())
    unique_location_general = list(mock_df.location_general.unique())

    mock_data_df = df[["seqName"]].copy()
    mock_data_df["strain_name"] = np.random.choice(
        unique_strain_names, len(mock_data_df)
    )
    mock_data_df["isolation_date"] = np.random.choice(
        unique_isolation_date, len(mock_data_df)
    )
    mock_data_df["location_general"] = np.random.choice(
        unique_location_general, len(mock_data_df)
    )

    return mock_data_df


def create_dummy_legend(categories_dict, cmap="tab20"):
    """
    Create a dummy legend to add to the plot.
    :param categories_dict: dict with the categories to plot.
    :param cmap: str. Colormap for the plot.
    :return: Adds a legend to the plot.
    """
    markers = [
        plt.Line2D(
            [0, 0],
            [0, 0],
            color=matplotlib.cm.get_cmap(cmap)(value),
            marker="o",
            linestyle="",
        )
        for value in categories_dict.values()
    ]
    plt.legend(markers, list(categories_dict.keys()), numpoints=1, prop={"size": 8})


def add_node_information(
    node,
    i,
    index,
    G,
    tree,
    cmap,
    categories_values,
    node_properties,
    group_by,
    cluster_info,
):
    """
    Add the node information to build the tree plot.
    :param node: int. Node index.
    :param i: int. iterator index.
    :param G: Graph.
    :param tree: MinimumSpanningTree object with the tree to plot.
    :param cmap: str. Plot colormap.
    :param categories_values: dict. Categories to plot.
    :param node_properties: dict containing the node properties: node_color, node_label, node_size.
    :param group_by: str. Classification of nodes: "lineage" or "cluster".
    :return: Updated graph and node properties.
    """
    G.nodes[i]["node"] = node
    G.nodes[i]["idx"] = index
    G.nodes[i]["lineage"] = tree.lineage_values[node]
    node_properties["node_label"].append(
        categories_values[node]
    )  # Go back to 0 to represent no differences
    if group_by == "lineage":
        node_properties["node_color"].append(
            matplotlib.cm.get_cmap(cmap)(categories_values[node])
        )
    else:
        node_properties["node_color"].append(categories_values[node])

    cluster_sequences, reversed_cluster_indices, clusters_assigned, _ = cluster_info
    node_properties["node_size"].append(
        len(cluster_sequences[reversed_cluster_indices[clusters_assigned[index]]])
        * PLOT_MULTIPLIER
    )
    return G, node_properties


def draw_tree(tree, cluster_info, group_by="lineage", cmap="tab20", legend=False):
    """
    Draw MST plot
    :param tree: MinimumSpanningTree object to draw
    :param group_by: str. How to categorize the nodes
    :param cmap: str. Plot colormap.
    :param legend:bool. True to add a legend to the plot
    :return: plot tree.
    """
    if group_by == "lineage":
        categories_dict = tree.lineage_categories
        categories_values = tree.lineage_values

    elif group_by == "cluster":
        clusters_tree = cluster_info
        sorted_clusters = dict(sorted(clusters_tree[2].items()))
        categories_dict = dict(
            zip(list(clusters_tree[0].keys()), list(clusters_tree[0].keys()))
        )
        categories_values = np.array(list(sorted_clusters.values()))

    else:
        raise Exception(
            "group_by parameter not supported. The only parameters supported are lineage and cluster"
        )

    # Create graph
    G = nx.Graph()

    # Add weighted edges
    edges = tree.from_matrix_to_list(tree.mst)
    G.add_weighted_edges_from(edges)

    # Create edge labels
    edge_labels = dict(
        [
            ((u, v,), d["weight"] - 1,)
            for u, v, d in G.edges(data=True)
            if d["weight"] > 0
        ]
    )

    node_properties = {}
    node_properties["node_label"] = []
    node_properties["node_color"] = []
    node_properties["node_size"] = []
    # Add properties to nodes
    for i, node in enumerate(list(G.nodes())):
        G, node_properties = add_node_information(
            node,
            i,
            list(cluster_info[2].keys())[i],
            G,
            tree,
            cmap,
            categories_values,
            node_properties,
            group_by,
            cluster_info=cluster_info,
        )

    # Start plot
    pos = nx.kamada_kawai_layout(G)
    plt.figure(figsize=(10, 10))

    if legend:
        if group_by == "lineage":
            # dummy plot for legend
            create_dummy_legend(categories_dict, cmap=cmap)

    # spanning tree plot
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_size=8)
    nx.draw_networkx(
        G,
        pos,
        with_labels=False,
        node_color=node_properties["node_color"],
        node_size=node_properties["node_size"],
        cmap=cmap,
    )

    pylab.show()


def get_mean_counts_per_cluster_lineage(df, columns):
    """
    Given a dataframe with cluster information, compute the mean counts per cluster, and number of clusters per lineage.

    Args:
        df (pandas.DataFrame): table containing the sequence and cluster information.
        columns (list): list of the column names as strings to compute the mean. ex: ["lineage", "clusters"].

    Returns:
        pandas.DataFrame: table containing the mean counts per cluster, and number of clusters per lineage information.
    """
    # Compute mean number of sequences per lineage
    new_df = df.groupby([columns[0], columns[1]]).size().to_frame().reset_index()

    # Get the mean counts per cluster per lineage
    mean_per_cluster_df = new_df[[columns[0], 0]].groupby([columns[0]]).mean()

    # Get the clusters per lineage count
    clusters_per_lineage_df = df.groupby([columns[0]]).size().to_frame().reset_index()

    # Add clusters per lineage to mean per cluster dataframe
    mean_per_cluster_df["cluster_counts"] = clusters_per_lineage_df[
        0
    ].values + np.random.uniform(
        low=-0.5, high=0.5, size=(len(clusters_per_lineage_df),)
    )
    mean_per_cluster_df = mean_per_cluster_df.rename(
        columns={0: "mean_cases_per_cluster"}
    ).reset_index()

    # Add x jitter
    mean_per_cluster_df.mean_cases_per_cluster = (
        mean_per_cluster_df.mean_cases_per_cluster
        + np.random.uniform(low=-0.5, high=0.5, size=(len(mean_per_cluster_df),))
    )
    mean_per_cluster_df.lineage = pd.Categorical(mean_per_cluster_df.lineage)
    mean_per_cluster_df["lineage_values"] = mean_per_cluster_df.lineage.cat.codes

    return mean_per_cluster_df


def plot_lineage_counts(mean_df, colx, coly, col_hue):
    """
    Plot the mean clusers per lineage vs the mean cluster counts in a scatterplot.

    Args:
        mean_df (pandas.DataFrame): Table containing the mean clusters information (from get_mean_counts_per_cluster_lineage()).
        colx (str): column name to go on x axis.
        coly (str): column name to go on y axis.
        col_hue (str): column name to be in the hue.
    """

    plt.title("Lineage cluster counts")
    with sns.axes_style("whitegrid"):
        sns.scatterplot(
            x=colx, y=coly, hue=col_hue, data=mean_df,
        )

    # Add scatter lineage labels
    for i in range(mean_df.shape[0]):
        plt.text(
            x=mean_df[colx][i] + 0.01,
            y=mean_df[coly][i] + 0.01,
            s=mean_df[col_hue][i],
            fontdict=dict(color="black", size=10),
        )
    # plt.ylim([0, mean_df[coly].max() + 5])
    plt.xlabel("Number of clusters per lineage")
    plt.ylabel("Average number of cases per cluster")
    plt.legend("")


def plot_lineage_data(final_dataframe, max_lineages=6):
    """
    Plot boxplot and barplot for clusters per lineage data analysis.
    :param tree: MinimumSpanningTree object.
    :param final_dataframe: pandas.DataFrame with all the data.
    :param max_lineages: int. Number of lineages to plot.
    :return: plots.
    """
    # Get list of lineages
    lineages = final_dataframe["assembly.annotation_pangolin.lineage"].unique()
    cluster_counts_per_lineage = []
    number_of_clusters = []

    # For each lineage get the number of clusters and sequences
    for lineage in lineages:
        temp_df = final_dataframe.loc[
            final_dataframe["assembly.annotation_pangolin.lineage"] == lineage
        ]
        clusters = temp_df.clusters.values
        (unique_clusters, counts_clusters) = np.unique(clusters, return_counts=True)
        cluster_counts_per_lineage.append([int(i) for i in counts_clusters])
        number_of_clusters.append(len(unique_clusters))
    new_pd = pd.DataFrame()
    new_pd["assembly.annotation_pangolin.lineage"] = lineages
    new_pd["cluster_counts"] = cluster_counts_per_lineage

    # Get the data for the lineages having the greater amount of clusters
    lineages_to_show = max_lineages
    if lineages_to_show < len(number_of_clusters):
        indices = np.argpartition(
            np.array(number_of_clusters),
            kth=-min(lineages_to_show, len(number_of_clusters) - 1),
        )[-min(lineages_to_show, len(number_of_clusters) - 1) :]
    else:
        indices = range(len(new_pd))

    indices_lineages = new_pd.iloc[indices][
        "assembly.annotation_pangolin.lineage"
    ].values
    new_pd = new_pd.iloc[indices].explode("cluster_counts")

    # mean_per_cluster_df = get_mean_counts_per_cluster_lineage(
    #    final_dataframe, ["lineage", "clusters"]
    # )

    # mean_per_cluster_df.to_csv("median_cases_clusters_per_lineage.csv", index=False)

    # Plot values
    plt.subplot(2, 1, 1)
    plt.title("Distribution of number of sequences per cluster per lineage")
    sns.boxplot(
        x="assembly.annotation_pangolin.lineage", y="cluster_counts", data=new_pd
    )
    plt.ylabel("Sequences per cluster")
    plt.xlabel("Lineages")

    plt.subplot(2, 1, 2)
    plt.title("Number of clustersper lineage")
    plt.xlabel("Lineage")
    plt.ylabel("Number of clusters")
    plt.bar(x=indices_lineages, height=np.array(number_of_clusters)[indices])


def filter_by_cluster_size(df, limit_up=np.inf, limit_low=0):
    """
    Filter the input dataframe according to the cluster size.

    Args:
        df (pandas.DataFrame): dataframe to filter
        limit_up (int, optional): Upper limmit to filter. Defaults to np.inf.
        limit_low (int, optional): Lower limit to filter. Defaults to 0.

    Returns:
        pandas.DataFrame: dataframe containing the values filtered by cluster size
    """
    print("Filtering by cluster size. \n")
    df_temp = df.copy()
    df_temp_na = df[df["query"] == 1]
    df_temp = df[df["query"] == 0]

    # Count the number of sequences per cluster
    clusters_count_df = (
        df_temp.groupby("clusters").count()[["sample.strain_name"]].reset_index()
    )
    clusters_count = clusters_count_df["sample.strain_name"].values

    # Get the clusters that fullfill the conditions
    filtered_clusters = np.where(
        (clusters_count <= limit_up) & (clusters_count >= limit_low)
    )[0]

    removed_clusters = np.where(
        (clusters_count > limit_up) | (clusters_count < limit_low)
    )[0]

    selected_clusters = clusters_count_df.iloc[filtered_clusters].clusters.values
    df_temp_new = df_temp.loc[df_temp.clusters.isin(selected_clusters)]

    removed_clusters_find = clusters_count_df.iloc[removed_clusters].clusters.values
    df_temp_removed = df_temp.loc[df_temp.clusters.isin(removed_clusters_find)]

    return pd.concat([df_temp_new, df_temp_na]), df_temp_removed


def filter_by_date(df, limit_up, limit_low):
    """
    Filter the input data by date and retrieve the sequences within the date range specified by the user.

    Args:
        df (pandas.DataFrame): dataframe to filter.
        limit_up (datetime): Latest date to include in the analysis.
        limit_low (datetime): Earliest date to include in the analysis.
        
    Returns:
        pandas.DataFrame: dataframe contianing the filtered values by date.
    """
    print("Filtering by date. \n")
    df_temp = df.copy()
    df_temp["sample.isolation_date"] = pd.to_datetime(df_temp["sample.isolation_date"])

    # Get samples within the date interval
    df_in_date = df_temp.loc[
        (df_temp["sample.isolation_date"] <= limit_up)
        & (df_temp["sample.isolation_date"] >= limit_low)
    ]
    df_not_in_date = df_temp.loc[
        (df_temp["sample.isolation_date"] > limit_up)
        | (df_temp["sample.isolation_date"] < limit_low)
    ]

    return df_in_date, df_not_in_date


def filter_by_days(df, limit_up=np.inf, limit_low=0):
    """
    Filter the clusters according to a maximum and minimum isolation_date span.

    Args:
        df (pandas.DataFrame): dataframe to filter
        limit_up (int, optional): Upper limmit to filter. Defaults to np.inf.
        limit_low (int, optional): Lower limit to filter. Defaults to 0.

    Returns:
        pandas.Dataframe: dataframe containing the values filtered by days.
    """
    print("Filtering by days. \n")
    df_temp = df.copy()
    df_temp["sample.isolation_date"] = pd.to_datetime(df_temp["sample.isolation_date"])
    df_temp_na = df[df["query"] == 1]
    df_temp = df[df["query"] == 0]

    # Get max dispersion dates per cluster
    clusters_date_span = []
    clusters_unique = df_temp.clusters.unique()
    for cluster in clusters_unique:
        date_min = df_temp.loc[df_temp.clusters == cluster][
            "sample.isolation_date"
        ].min()
        date_max = df_temp.loc[df_temp.clusters == cluster][
            "sample.isolation_date"
        ].max()

        # Get date difference
        diff = date_max - date_min
        clusters_date_span.append(diff.days)

    filtered_clusters = np.where(
        (np.array(clusters_date_span) <= limit_up)
        & (np.array(clusters_date_span) >= limit_low)
    )[0]

    removed_clusters = np.where(
        (np.array(clusters_date_span) > limit_up)
        | (np.array(clusters_date_span) < limit_low)
    )[0]

    isin = df_temp.clusters.isin(clusters_unique[filtered_clusters])
    df_temp_new = df_temp.loc[isin]

    isin_removed = df_temp.clusters.isin(clusters_unique[removed_clusters])
    df_temp_removed = df_temp.loc[isin_removed]

    return pd.concat([df_temp_new, df_temp_na]), df_temp_removed


def filter_by_location(df, location):
    """
    Filter df entries by the values of the location_general column of df.

    Args:
        df (pandas.DataFrame): Table containing the values to filter.
        location (str): Value to filter

    Returns:
        pandas.DataFrame: Filtered df.
    """

    return (
        df.loc[df["sample_origin_location.country_division"].isin(location)],
        df.loc[df[~"sample_origin_location.country_division"].isin(location)],
    )


def read_files(
    pangolin_file_in, nextclade_file_in, mock_data_file_in, real_datafile=True
):
    """
    Read the input information

    Args:
        pangolin_file_in (str): Path to the pangolin file.
        nextclade_file_in (str): Path to the nextclade file.
        mock_data_file_in (str): Path to the samples file
        real_datafile (bool, optional): Describe if the samples file contains real or mock data. Defaults to True.

    Returns:
        pandas.DataFrame: table with the merged information for the analysis.
    """
    pangolin_df = pd.read_csv(pangolin_file_in, sep="\t")  # .sample(100)
    nextclade_df = pd.read_csv(nextclade_file_in, sep="\t")
    mock_data_df = pd.read_csv(mock_data_file_in, sep="\t")

    # Merge pangolin and nextclade datasets
    # Get only the important columns from pangolin_df
    df = pangolin_df[
        [
            "sample.strain_name",
            "assembly.annotation_pangolin.lineage",
            "assembly.annotation_pangolin.scorpio_call",
        ]
    ].copy()
    # df = df.rename(columns={"taxon": "seqName"})
    # mock_data_df = mock_data_df.rename(columns={"strain_name": "seqName"})

    # Merge
    df = df.merge(
        nextclade_df[
            [
                "sample.strain_name",
                "assembly.annotation_next_clade.substitutions",
                "assembly.annotation_next_clade.aa_substitutions",
            ]
        ],
        on="sample.strain_name",
    )

    # Create substitutions lists
    df["substitutions_list"] = df.apply(
        lambda row: sorted(
            row["assembly.annotation_next_clade.substitutions"].split(",")
        )
        if isinstance(row["assembly.annotation_next_clade.substitutions"], str)
        else [],
        axis=1,
    )
    df["aaSubstitutions_list"] = df.apply(
        lambda row: sorted(
            row["assembly.annotation_next_clade.aa_substitutions"].split(",")
        )
        if isinstance(row["assembly.annotation_next_clade.aa_substitutions"], str)
        else [],
        axis=1,
    )

    # Create mock location dataframe
    if not real_datafile:
        mock_dataframe = create_mock_dataset(mock_data_df, df)
    else:
        mock_dataframe = mock_data_df

    return df, mock_dataframe


def get_cluster_df(df, clusters_1snp_info, cluster_info):
    """
    Gets the 0-SNP and 1-SNP information for each cluster.

    Args:
        df (pandas.DataFrame): table containing the initial sequence information.
        clusters_1snp_info (dictionary): dictionary containing the information for the 1-SNP clusters.
        cluster_info (dictionary): dictionary containing the information for the 0-SNP clusters.
    Returns:
        pandas.DataFrame: Table summarizing the information of each cluster.
    """
    cluster_info_dict = {
        "cluster_id": [],
        "num_cases(0)": [],
        "lineage(0)": [],
        "start_date(0)": [],
        "end_date(0)": [],
        "cantons(0)": [],
        "query(0)": [],
        "num_cases(1)": [],
        "lineage(1)": [],
        "start_date(1)": [],
        "end_date(1)": [],
        "cantons(1)": [],
        "query(1)": [],
    }
    for cluster in df.clusters.unique():
        temp_df = df.loc[df.clusters == cluster].copy()
        cluster_info_dict["cluster_id"].append(cluster)
        cluster_info_dict["num_cases(0)"].append(len(temp_df))
        cluster_info_dict["lineage(0)"].append(
            ",".join(list(temp_df["assembly.annotation_pangolin.lineage"].unique()))
        )
        cluster_info_dict["start_date(0)"].append(
            temp_df["sample.isolation_date"].min()
        )
        cluster_info_dict["end_date(0)"].append(temp_df["sample.isolation_date"].max())
        cluster_info_dict["cantons(0)"].append(
            ",".join(
                temp_df.loc[temp_df["sample_origin_location.country_division"].notna()][
                    "sample_origin_location.country_division"
                ].unique()
            )
        )
        cluster_info_dict["query(0)"].append(temp_df["query"].unique())

        # Get the information for 1SNP clusters
        cluster_sequence = cluster_info[1][cluster]
        cluster_indices = clusters_1snp_info[cluster_sequence]
        df_1snp = df.loc[df.index.isin(cluster_indices)].copy()
        cluster_info_dict["num_cases(1)"].append(len(df_1snp))
        cluster_info_dict["lineage(1)"].append(
            ",".join(list(df_1snp["assembly.annotation_pangolin.lineage"].unique()))
        )
        cluster_info_dict["start_date(1)"].append(
            df_1snp["sample.isolation_date"].min()
        )
        cluster_info_dict["end_date(1)"].append(df_1snp["sample.isolation_date"].max())
        cluster_info_dict["cantons(1)"].append(
            ",".join(
                df_1snp.loc[df_1snp["sample_origin_location.country_division"].notna()][
                    "sample_origin_location.country_division"
                ].unique()
            )
        )
        cluster_info_dict["query(1)"].append(df_1snp["query"].unique())

    return pd.DataFrame(cluster_info_dict)


def compute_snp_differences(seq1, seq2):
    """
    Computes the number of differences between the two input lists

    Args:
        seq1 (list): list containing values to be compared with seq2.
        seq2 (list): list containing values to be compared with seq1.

    Returns:
        int: number of differences between the input sequences.
    """
    # Compute similarities between both rows of values
    # similarities = [sim for sim in seq1 if sim in seq2]
    # Get the differences
    # diff_i = len(seq1) - len(similarities)
    # diff_j = len(seq2) - len(similarities)
    # return diff_i + diff_j

    # Compute similarities between both rows of values
    similarities = set(seq1).intersection(set(seq2))
    # Get the differences
    diff_i = len(seq1) - len(similarities)
    diff_j = len(seq2) - len(similarities)
    return diff_i + diff_j


def create_multifasta(fasta_directory_path, pattern="*.fasta*"):
    """
    Create a multifasta file from all the fasta files found in the given directory. The resulting file will be named  directoryName_multifasta.fasta

    Args:
        fasta_directory_path (str): path to the directory containing the fasta files to process.
        pattern (str, optional): Extension of the fasta file, eg. "*.fasta*", "*.fa". Defaults to "*.fasta*".

    Returns:
        str: path to the generated multifasta file
    """

    fasta_directory = pathlib.Path(fasta_directory_path)

    # Define the pattern that defines the fasta files
    current_pattern = pattern
    data = ""

    # If multifasta in folder -> skip multifasta creation

    for _, _, files in os.walk(fasta_directory_path):
        if any("multifasta" in file for file in files):
            multifasta_filename = [file for file in files if "multifasta" in file][0]
            print(
                "Found multifasta file in input directory: %s \n" % multifasta_filename
            )
            return os.path.join(fasta_directory, multifasta_filename)

    # Find all the files that match the pattern for the fasta files
    print("Generating the multifasta file ...")
    for current_file in fasta_directory.glob(current_pattern):
        with open(current_file) as fp:
            data += fp.read()

    path, directory_name = os.path.split(fasta_directory)
    multifasta_filename = os.path.join(
        fasta_directory, "%s_multifasta.fasta" % directory_name
    )
    with open(multifasta_filename, "w") as fp:
        fp.write(data)

    return multifasta_filename


def execute_nextclade(fasta_file, output_directory, data_directory="nextclade_data"):
    """
    Execute nextclade on selected fasta files.

    Args:
        fasta_file (str): Path to the fasta file.
        output_directory (str): Path where to store the output info.
        data_directory (str, optional): Directory where to find the sars-cov-2 nextclade information. Defaults to "nextclade_data".
    """
    out = subprocess.run(
        "nextclade \
                    --in-order \
                    --input-fasta={}\
                    --input-dataset={} \
                    --output-tsv={} \
                    --output-tree={} \
                    --output-dir={} \
                    --output-basename={}".format(
            fasta_file,
            os.path.join(data_directory, "sars-cov-2"),
            os.path.join(output_directory, "output_nextclade", "nextclade.tsv"),
            os.path.join(
                output_directory, "output_nextclade", "nextclade.auspice.json"
            ),
            os.path.join(output_directory, "output_nextclade"),
            "output_nextclade",
        ),
        shell=True,
        capture_output=True,
    )
    print("Error nextclade: ", out.stderr)
    print("Out nextclade: ", out.stderr)
    print(out, "\n")
    print("--- Done running nextclade.\n")


def execute_pangolin(fasta_file):
    """
    Execut pangolin on the selected fasta files.

    Args:
        fasta_file (str): Path to the fasta file to use.

    Returns:
        pangolin output and stderr.
    """
    out = subprocess.run(
        "pangolin {}".format(fasta_file), shell=True, capture_output=True
    )

    print("Error pangolin: ", out.stderr)
    print("Out pangolin: ", out.stderr)
    print("--- Done running pangolin.\n")
    return out


def merge_string(list_to_merge):
    """
    Merge a list of strings into a single string.

    Args:
        list_to_merge (list): List of the strings to merge.

    Returns:
        str: merged string.
    """
    st_out = ""
    for st in list_to_merge:
        sub = st.split("/")[-1]
        st_out = st_out + sub
    return st_out


def jaccard_index_matrix(features):
    """
    Create a matrix that contains the jaccard index between all sequences stated in features.

    Args:
        features (numpy.ndarray): 2d array with the features to compare.

    Returns:
        numpy.ndarray: Matrix with the jaccard index between all sequences.
    """
    shape = features.shape[0]
    jaccard_matrix = np.zeros((shape, shape))
    for i in tqdm(range(shape)):
        for j in range(0 + i, shape):
            if i == j:
                jaccard_matrix[i, j] = 1
                continue
            jaccard_matrix[i, j] = jaccard_score(
                features[i, :], features[j, :], zero_division=0.0
            )
            jaccard_matrix[j, i] = jaccard_score(
                features[i, :], features[j, :], zero_division=0.0
            )
    return jaccard_matrix
